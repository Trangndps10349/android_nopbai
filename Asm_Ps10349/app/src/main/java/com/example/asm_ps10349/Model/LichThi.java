package com.example.asm_ps10349.Model;

public class LichThi {
    private String MaLichThi;
    private String TenMonThi;
    private String NgayThi;
    private String MaMonThi;

    public LichThi() {
    }

    public String getMaLichThi() {
        return MaLichThi;
    }

    public void setMaLichThi(String maLichThi) {
        MaLichThi = maLichThi;
    }

    public String getTenMonThi() {
        return TenMonThi;
    }

    public void setTenMonThi(String tenMonThi) {
        TenMonThi = tenMonThi;
    }

    public String getMaMonThi() {
        return MaMonThi;
    }

    public void setMaMonThi(String maMonThi) {
        MaMonThi = maMonThi;
    }

    public String getNgayThi() {
        return NgayThi;
    }

    public void setNgayThi(String ngayThi) {
        NgayThi = ngayThi;
    }

    public LichThi(String maLichThi, String tenMonThi, String ngayThi,String maMonThi) {
        MaLichThi = maLichThi;
        TenMonThi = tenMonThi;
        NgayThi = ngayThi;
        MaMonThi = maMonThi;
    }

    @Override
    public String toString() {
        return "LichThi{" +
                "MaLichThi='" + MaLichThi + '\'' +
                ", TenMonThi='" + TenMonThi + '\'' +
                ", NgayThi='" + NgayThi + '\'' +
                ", MaMonThi='" + MaMonThi + '\'' +
                '}';
    }
}
