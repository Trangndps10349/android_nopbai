package com.example.asm_ps10349.Fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.asm_ps10349.Adapter.AdapterLichThi;
import com.example.asm_ps10349.Adapter.AdapterSp;
import com.example.asm_ps10349.Database.LichThiDao;
import com.example.asm_ps10349.Database.MonHocDao;
import com.example.asm_ps10349.Model.LichThi;
import com.example.asm_ps10349.Model.MonHoc;
import com.example.asm_ps10349.R;

import java.util.ArrayList;

public class XemLichThiFragment extends Fragment {
private ArrayList<LichThi>dsLichThi;
private ArrayList<MonHoc>dsMonHoc;
private EditText edtIdLichThi,edtTenLichThi;
private TextView tvChonNgay;
private Spinner spinner;
private AdapterLichThi adapterLichThi;
private AdapterSp adapterSp;
private Button btnThem,btnSua;
private LichThiDao lichThiDao;
private MonHocDao monHocDao;
private ListView lvLichThi;


    public static XemLichThiFragment newInstance() {
        Bundle args = new Bundle();
        XemLichThiFragment fragment = new XemLichThiFragment();
        fragment.setArguments(args);
        return fragment;
    }
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_xem_lich_thi,container,false);
        dsLichThi = new ArrayList<>();
        dsMonHoc = new ArrayList<>();

        lichThiDao = new LichThiDao(getContext());
        monHocDao = new MonHocDao(getContext());

        dsMonHoc = (ArrayList<MonHoc>) monHocDao.getAllMonHoc();
        dsLichThi = (ArrayList<LichThi>) lichThiDao.getAllLichThi();
        adapterSp = new AdapterSp(getContext(),dsMonHoc);
        spinner.setAdapter(adapterSp);

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                int mPosition = spinner.getSelectedItemPosition();
                MonHoc monHoc = dsMonHoc.get(mPosition);
                final int txtMaMon = Integer.parseInt(monHoc.getId());
                dsLichThi = (ArrayList<LichThi>) lichThiDao.getAllLichThi();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        return v;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }
}
