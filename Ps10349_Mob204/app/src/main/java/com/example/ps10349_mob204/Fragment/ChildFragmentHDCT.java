package com.example.ps10349_mob204.Fragment;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.ps10349_mob204.Adapter.AdapterHDCT;
import com.example.ps10349_mob204.Adapter.SpHDCTAdapter;
import com.example.ps10349_mob204.Dao.HoaDonChiTietDao;
import com.example.ps10349_mob204.Dao.HoaDonDao;
import com.example.ps10349_mob204.Dao.SachDao;
import com.example.ps10349_mob204.Model.HoaDon;
import com.example.ps10349_mob204.Model.HoaDonChiTiet;
import com.example.ps10349_mob204.Model.Sach;
import com.example.ps10349_mob204.R;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;


import static com.example.ps10349_mob204.R.id.btn_them_hdct;

public class ChildFragmentHDCT extends Fragment {
    AdapterHDCT adapterHDCT;
    RecyclerView recyclerViewHDCT;

    ArrayList<HoaDon>hoaDonList;
    ArrayList<HoaDonChiTiet>hoaDonChiTietList;

    HoaDonDao hoaDonDao;
    HoaDonChiTietDao hoaDonChiTietDao;
    SachDao sachDao;

    Spinner spinner;
    SpHDCTAdapter spHDCTAdapter;

    EditText edtMaSach,edtSoLuong;
    Button btnThem,btnThanhToan;





    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.child_fragment_hdct, container, false);

        hoaDonList= new ArrayList<>();
        hoaDonChiTietList = new ArrayList<>();

        hoaDonDao = new HoaDonDao(getContext());
        hoaDonChiTietDao = new HoaDonChiTietDao(getContext());


        hoaDonChiTietList = (ArrayList<HoaDonChiTiet>) hoaDonChiTietDao
                .getAllHoaDonChiTiet();
        try {
            hoaDonList = (ArrayList<HoaDon>) hoaDonDao.getAllHoaDon();
        } catch (ParseException e) {
            e.printStackTrace();
        }

        recyclerViewHDCT = view.findViewById(R.id.recyclerview_hdct);
        edtMaSach = view.findViewById(R.id.edtMaSach);
        edtSoLuong = view.findViewById(R.id.edtSoLuongMua);
        adapterHDCT = new AdapterHDCT(hoaDonChiTietList);
        //spinner
        spinner = view.findViewById(R.id.sp_hdct);
        spHDCTAdapter = new SpHDCTAdapter(getContext(),hoaDonList);
        spinner.setAdapter(spHDCTAdapter);




        recyclerViewHDCT.setAdapter(adapterHDCT);
        recyclerViewHDCT.setLayoutManager(new LinearLayoutManager(getContext()));
        registerForContextMenu(recyclerViewHDCT);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        btnThem = view.findViewById(btn_them_hdct);
        btnThanhToan = view.findViewById(R.id.btn_thanh_toan_hdct);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                int mPosition = spinner.getSelectedItemPosition();
                HoaDon hoaDon = hoaDonList.get(mPosition);
                final int txtMaHoaDon = Integer.parseInt((hoaDon.getMaHoaDon()));
                hoaDonChiTietList.clear();
                hoaDonChiTietList = (ArrayList<HoaDonChiTiet>) hoaDonChiTietDao
                        .getAllHoaDonChiTietById(String.valueOf(txtMaHoaDon));
                adapterHDCT.setListHDCT(hoaDonChiTietList);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        btnThem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addHoaDonChiTiet();
                capNhatGiaoDienHDCT();
            }
        });
    }
    public void capNhatGiaoDienHDCT(){
        int mPosition = spinner.getSelectedItemPosition();
        HoaDon hoaDon = hoaDonList.get(mPosition);
        final int txtMaHoaDon = Integer.parseInt((hoaDon.getMaHoaDon()));
        hoaDonChiTietList.clear();
        hoaDonChiTietList = (ArrayList<HoaDonChiTiet>) hoaDonChiTietDao
                .getAllHoaDonChiTietById(String.valueOf(txtMaHoaDon));
        adapterHDCT.setListHDCT(hoaDonChiTietList);


    }
    public void addHoaDonChiTiet() {
        HoaDon hoaDon = new HoaDon();
        hoaDonChiTietDao = new HoaDonChiTietDao(getContext());
        hoaDonDao = new HoaDonDao(getContext());
        sachDao = new SachDao(getContext());
        spinner.getSelectedItem();
        try{
            if (validation() < 0) {
                Toast.makeText(getContext(), "Vui lòng nhập đầy đủ thông tin", Toast.LENGTH_SHORT).show();
            }else {
                Sach sach = sachDao.getSachById(edtMaSach.getText().toString());
                if (sach!=null){
                    int pos = checkMaSach(hoaDonChiTietList,edtMaSach.getText().toString());
                    HoaDonChiTiet hoaDonChiTiet = new HoaDonChiTiet(
                            1,hoaDon,sach,Integer.parseInt(edtSoLuong.getText().toString()));
                    if (pos>=0){
                        int soluong = hoaDonChiTietList.get(pos).getSoLuongMua();
                        hoaDonChiTiet.setSoLuongMua(soluong + Integer.parseInt(edtSoLuong.getText().toString()));
                        hoaDonChiTietList.set(pos,hoaDonChiTiet);
                    }else {
                        Toast.makeText(getContext(),"Mã sách không tồn tại",Toast.LENGTH_SHORT).show();                 }
                }
            }
            }catch (Exception ex){
            Log.e("===============",ex.toString());
        }

    }
        public int validation(){
        if (edtMaSach.getText().toString().isEmpty()||
                edtSoLuong.getText().toString().isEmpty()
                )
        {
            return -1;
        }
        return 1;
    }
    public int checkMaSach(List<HoaDonChiTiet> hoaDonChiTietList, String maSach){
        int pos = -1;
        for (int i = 0; i < hoaDonChiTietList.size(); i++){
            HoaDonChiTiet hd = hoaDonChiTietList.get(i);
            if (hd.getSach().getMaSach().equalsIgnoreCase(maSach)){
                pos = i;
                break;
            }
        }
        return pos;
    }

}

