package com.example.ps10349_mob204.Model;

import java.util.Date;

public class HoaDon {
    private String maHoaDon;
    private Date ngayMuaHang;

    public HoaDon(String maHoaDon, Date ngayMuaHang) {
        this.maHoaDon = maHoaDon;
        this.ngayMuaHang = ngayMuaHang;
    }

    public HoaDon() {
    }

    public String getMaHoaDon() {
        return maHoaDon;
    }

    public void setMaHoaDon(String maHoaDon) {
        this.maHoaDon = maHoaDon;
    }

    public Date getNgayMuaHang() {
        return ngayMuaHang;
    }

    public void setNgayMuaHang(Date ngayMuaHang) {
        this.ngayMuaHang = ngayMuaHang;
    }

    @Override
    public String toString() {
        return "HoaDon{" +
                "maHoaDon='" + maHoaDon + '\'' +
                ", ngayMuaHang='" + ngayMuaHang + '\'' +
                '}';
    }
}
