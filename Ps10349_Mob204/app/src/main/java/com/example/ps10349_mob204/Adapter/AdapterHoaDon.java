package com.example.ps10349_mob204.Adapter;

import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.ps10349_mob204.Model.HoaDon;
import com.example.ps10349_mob204.R;

import java.util.ArrayList;
import java.util.List;

public class AdapterHoaDon extends RecyclerView.Adapter<AdapterHoaDon.ViewHolder> {
    private List<HoaDon> dsHoaDon;
    private int position = -1;

    public AdapterHoaDon(List<HoaDon> dsHoaDon) {
        this.dsHoaDon = dsHoaDon;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View contactView = layoutInflater.inflate(R.layout.item_hoa_don, parent, false);
        return new ViewHolder(contactView);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int position) {
        HoaDon hoaDon = dsHoaDon.get(position);
        holder.tvMaHD.setText(hoaDon.getMaHoaDon());
        holder.tvNgayHD.setText("" + hoaDon.getNgayMuaHang());
        holder.itemView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                setPosition(holder.getAdapterPosition());
                return false;
            }
        });

    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    @Override
    public int getItemCount() {
        return dsHoaDon.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder
            implements View.OnCreateContextMenuListener {

        TextView tvMaHD, tvNgayHD;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            tvMaHD = itemView.findViewById(R.id.tv_ma_hoa_don);
            tvNgayHD = itemView.findViewById(R.id.tv_ngay_hoa_don);
            itemView.setOnCreateContextMenuListener(this);
        }

        @Override
        public void onCreateContextMenu(ContextMenu contextMenu, View view,
                                        ContextMenu.ContextMenuInfo contextMenuInfo) {
            contextMenu.add(Menu.NONE, R.id.edit_hd,
                    Menu.NONE, R.string.edit);

            contextMenu.add(Menu.NONE, R.id.delete_hd,
                    Menu.NONE, R.string.delete);
        }
    }

    public void setListHoaDon(List<HoaDon> danhSachHoaDon) {
        if (danhSachHoaDon != null) {
            this.dsHoaDon = new ArrayList<>(danhSachHoaDon);
            notifyDataSetChanged();
        }
    }
}
