package com.example.nestedrecyclerview.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.nestedrecyclerview.Model.ItemData;
import com.example.nestedrecyclerview.Model.ItemGroup;
import com.example.nestedrecyclerview.R;

import java.util.List;

public class ItemGroupAdapter extends RecyclerView.Adapter<ItemGroupAdapter.ViewHolder> {
    private Context context;
    private List<ItemGroup>dataList;

    public ItemGroupAdapter(Context context, List<ItemGroup> dataList) {
        this.context = context;
        this.dataList = dataList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_group,parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int position) {
        holder.tvTitleGroup.setText(dataList.get(position).getHeaderTitle());
        List<ItemData>itemData = dataList.get(position).getListItem();
        ItemAdapter itemAdapter = new ItemAdapter(context,itemData);

        holder.recyclerView_item_group.setHasFixedSize(true);
        holder.recyclerView_item_group.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL,false));
        holder.recyclerView_item_group.setAdapter(itemAdapter);
        holder.recyclerView_item_group.setNestedScrollingEnabled(false);//important
        // button more
        holder.btnMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(context, "More"+holder.tvTitleGroup.getText(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public int getItemCount() {
        return (dataList != null ? dataList.size() : 0);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView tvTitleGroup;
        RecyclerView recyclerView_item_group;
        Button btnMore;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            tvTitleGroup = (TextView)itemView.findViewById(R.id.tvTittle);
            btnMore = itemView.findViewById(R.id.btn_more);
            recyclerView_item_group = itemView.findViewById(R.id.my_recycle_view_list);

        }
    }
}
