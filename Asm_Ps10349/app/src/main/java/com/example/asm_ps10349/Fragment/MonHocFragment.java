package com.example.asm_ps10349.Fragment;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.asm_ps10349.Adapter.AdapterMonHoc;
import com.example.asm_ps10349.Database.MonHocDao;
import com.example.asm_ps10349.Model.MonHoc;
import com.example.asm_ps10349.R;

import java.util.ArrayList;

public class MonHocFragment extends Fragment {
    private ArrayList<MonHoc> dsMonHoc;
    AdapterMonHoc adapterMonHoc;
    private MonHoc monHoc;
    private MonHocDao monHocDao;
    private ListView lvMonHoc;
    private EditText edtMaMon, edtTenMon, edtLichHoc;
    private Button btnThem, btnSua;

    public static MonHocFragment newInstance() {
        Bundle args = new Bundle();
        MonHocFragment fragment = new MonHocFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_mon_hoc, container, false);
        edtMaMon = v.findViewById(R.id.edt_ma_mon);
        edtTenMon = v.findViewById(R.id.edt_ten_mon);
        btnSua = v.findViewById(R.id.btn_sua);
        btnThem = v.findViewById(R.id.btn_them);
        lvMonHoc = v.findViewById(R.id.lv_mon_hoc);
        edtLichHoc = v.findViewById(R.id.edt_lich_hoc);
        monHocDao = new MonHocDao(getContext());
        capNhatGiaoDien();

        return v;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        btnThem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                monHoc = new MonHoc();
                String txtMaMon = edtMaMon.getText().toString();
                String txtTenMon = edtTenMon.getText().toString();
                String txtLichHoc = edtLichHoc.getText().toString();
                if (TextUtils.isEmpty(txtMaMon) || TextUtils.isEmpty(txtTenMon) || TextUtils.isEmpty(txtLichHoc)) {
                    Toast.makeText(getContext(), "Thong tin can duoc nhap day du", Toast.LENGTH_SHORT).show();
                } else {
                    monHoc.setId(txtMaMon);
                    monHoc.setTenMonHoc(txtTenMon);
                    monHoc.setLichHoc(txtLichHoc);
                    if (monHocDao.ThemMonHoc(monHoc) == -1) {
                        Toast.makeText(getContext(), "Them khong thanh cong", Toast.LENGTH_SHORT).show();
                    } else {
                        dsMonHoc.add(monHoc);
                        capNhatGiaoDien();
                        Toast.makeText(getContext(), "Them thanh cong", Toast.LENGTH_SHORT).show();
                        edtMaMon.setText("");
                        edtTenMon.setText("");
                        edtLichHoc.setText("");
                    }
                }
            }
        });
        btnSua.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                monHoc = new MonHoc();

                String txtMaMon = edtMaMon.getText().toString();
                String txtTenMon = edtTenMon.getText().toString();
                String txtLichHoc = edtLichHoc.getText().toString();


                monHoc.setId(txtMaMon);
                monHoc.setTenMonHoc(txtTenMon);
                monHoc.setLichHoc(txtLichHoc);

                if (monHocDao.suaMonHoc(monHoc) == -1) {
                    Toast.makeText(getContext(), "Khong duoc sua ma mon", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(getContext(), "Sua thanh cong", Toast.LENGTH_SHORT).show();

                    capNhatGiaoDien();

                    edtMaMon.setText("");
                    edtTenMon.setText("");
                    edtLichHoc.setText("");
                }


            }
        });
    }

    private void capNhatGiaoDien() {
        dsMonHoc = (ArrayList<MonHoc>) monHocDao.getAllMonHoc();
        adapterMonHoc = new AdapterMonHoc(getContext(), dsMonHoc, new AdapterMonHoc.MonHocListener() {
            @Override
            public void xoaMonHoc(String idMaMon) {

                monHocDao = new MonHocDao(getContext());
                monHocDao.xoaMonHoc(idMaMon);
                capNhatGiaoDien();
            }

            @Override
            public void suaMonHoc(MonHoc monHoc) {
                if (monHoc != null) {
                    edtMaMon.setText(monHoc.getId());
                    edtTenMon.setText(monHoc.getTenMonHoc());
                    edtLichHoc.setText(monHoc.getLichHoc());

                } else {
                    edtMaMon.setText("");
                    edtTenMon.setText("");
                    edtLichHoc.setText("");
                }
            }
        }) {

        };
        lvMonHoc.setAdapter(adapterMonHoc);
    }
}
