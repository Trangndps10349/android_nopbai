package com.example.ps10349_mob204.Fragment;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.ps10349_mob204.Adapter.AdapterHoaDon;
import com.example.ps10349_mob204.Dao.HoaDonDao;
import com.example.ps10349_mob204.Model.HoaDon;
import com.example.ps10349_mob204.R;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class ChildFragmentHoaDon  extends Fragment {
    HoaDonDao hoaDonDao;
    AdapterHoaDon adapterHoaDon;
    RecyclerView recyclerViewHoaDon;
    ArrayList<HoaDon> dsHoaDon;
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.child_fragment_hoa_don, container, false);

        dsHoaDon = new ArrayList<>();
        hoaDonDao = new HoaDonDao(getContext());
        recyclerViewHoaDon = view.findViewById(R.id.recyclerview_hoa_don);
        recyclerViewHoaDon.setLayoutManager(new LinearLayoutManager(getContext()));
        adapterHoaDon = new AdapterHoaDon(dsHoaDon);
        recyclerViewHoaDon.setAdapter(adapterHoaDon);


        try {
            capNhatGiaoDienHoaDon();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        registerForContextMenu(recyclerViewHoaDon);

        return view;

    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        view.findViewById(R.id.fab_them_hd).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                themHdDialog();
            }
        });

    }

    @Override
    public boolean onContextItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.edit_hd:
                if (dsHoaDon != null
                        && dsHoaDon.size() > adapterHoaDon.getPosition()
                        && adapterHoaDon.getPosition() >= 0) {

                    Toast.makeText(getContext(), "Khong dc sua hoa don", Toast.LENGTH_SHORT).show();
                }
                return true;
            case R.id.delete_hd:
                if (dsHoaDon != null
                        && dsHoaDon.size() > adapterHoaDon.getPosition()
                        && adapterHoaDon.getPosition() >= 0) {
                            xoaDialogHoaDon();
                }
                return true;
        }
        return super.onContextItemSelected(item);
    }

    private void themHdDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getLayoutInflater().getContext());
        final LayoutInflater inflater = getActivity().getLayoutInflater();
        View v = inflater.inflate(R.layout.dialog_them_hoa_don, null);

        final EditText edtMaHd = v.findViewById(R.id.edt_them_ma_hd);
        final TextView tvNgayMua = v.findViewById(R.id.tv_them_ngay_hd);

        builder.setTitle("THEM HOA DON");
        builder.setView(v);

        tvNgayMua.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Calendar calendar = Calendar.getInstance();
                int day = calendar.get(Calendar.DAY_OF_MONTH);
                int month = calendar.get(Calendar.MONTH);
                int year = calendar.get(Calendar.YEAR);
                DatePickerDialog datePickerDialog = new DatePickerDialog(getContext(), new DatePickerDialog.OnDateSetListener() {
                    @SuppressLint("SetTextI18n")
                    @Override
                    public void onDateSet(DatePicker datePicker, int year, int month, int day) {
                        //i: nam, i1: thang, i2 Ngay
                        //calendar.set(day, month, year);
                        //Log.i("==========",)
                        Calendar calendar = Calendar.getInstance();
                        calendar.set(Calendar.YEAR, year);
                        calendar.set(Calendar.MONTH, month);
                        calendar.set(Calendar.DAY_OF_MONTH, day);
                        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy", Locale.getDefault());
                        tvNgayMua.setText(simpleDateFormat.format(calendar.getTime()));
                    }
                }, year, month ,day );
                datePickerDialog.show();
            }
        });

        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                            HoaDon hoaDon = new HoaDon();
                            hoaDonDao = new HoaDonDao(getContext());
                            hoaDon.setMaHoaDon(edtMaHd.getText().toString());
                            try {
                                hoaDon.setNgayMuaHang(simpleDateFormat.parse(tvNgayMua.getText().toString()));
                            } catch (ParseException e) {
                                e.printStackTrace();
                            }
                            if (hoaDonDao.inserHoaDon(hoaDon)==-1){
                                Toast.makeText(getContext(), "Them ko thanh cong", Toast.LENGTH_SHORT).show();
                            }else {
                                dsHoaDon.add(hoaDon);
                                Log.e("-------",dsHoaDon.toString());
                                try {
                                    capNhatGiaoDienHoaDon();
                                } catch (ParseException e) {
                                    e.printStackTrace();
                                }
                                Toast.makeText(getContext(), "Them thanh cong", Toast.LENGTH_SHORT).show();
                            }
                        }
      });
        builder.setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    private void SuaDialogHoaDon(HoaDon hoaDon){
        AlertDialog.Builder builder = new AlertDialog.Builder(getLayoutInflater().getContext());
        final LayoutInflater inflater = getActivity().getLayoutInflater();
        View v = inflater.inflate(R.layout.dialog_sua_hoa_don, null);

        final EditText edtMaHd = v.findViewById(R.id.edt_sua_ma_hd);
        final TextView tvNgayMua = v.findViewById(R.id.tv_sua_ngay_hd);

        if (hoaDon != null) {
            edtMaHd.setText(hoaDon.getMaHoaDon());
            tvNgayMua.setText(simpleDateFormat.format(hoaDon.getNgayMuaHang()));
        } else {
            edtMaHd.setText(" ");
            tvNgayMua.setText(simpleDateFormat.format(hoaDon.getNgayMuaHang()));
        }
        tvNgayMua.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Calendar calendar = Calendar.getInstance();
                int day = calendar.get(Calendar.DAY_OF_MONTH);
                int month = calendar.get(Calendar.MONTH);
                int year = calendar.get(Calendar.YEAR);
                DatePickerDialog datePickerDialog = new DatePickerDialog(getContext(), new DatePickerDialog.OnDateSetListener() {
                    @SuppressLint("SetTextI18n")
                    @Override
                    public void onDateSet(DatePicker datePicker, int year, int month, int day) {
                        //i: nam, i1: thang, i2 Ngay
                        //calendar.set(day, month, year);
                        //Log.i("==========",)
                        Calendar calendar = Calendar.getInstance();
                        calendar.set(Calendar.YEAR, year);
                        calendar.set(Calendar.MONTH, month);
                        calendar.set(Calendar.DAY_OF_MONTH, day);
                        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy", Locale.getDefault());
                        tvNgayMua.setText(simpleDateFormat.format(calendar.getTime()));
                    }
                }, year, month ,day );
                datePickerDialog.show();
            }
        });

        builder.setTitle("SUA HOA DON");
        builder.setView(v);

        builder.setPositiveButton("ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                HoaDon hoaDon = new HoaDon();
                hoaDonDao = new HoaDonDao(getContext());
                hoaDon.setMaHoaDon(edtMaHd.getText().toString());
                try {
                    hoaDon.setNgayMuaHang(simpleDateFormat.parse(tvNgayMua.getText().toString()));
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                if (hoaDonDao.updateHoaDon(hoaDon)==-1){
                    Toast.makeText(getContext(), "Sua ko thanh cong", Toast.LENGTH_SHORT).show();
                }else {
                    dsHoaDon.add(hoaDon);
                    try {
                        capNhatGiaoDienHoaDon();
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    Toast.makeText(getContext(), "Sua thanh cong", Toast.LENGTH_SHORT).show();
                }
            }

        });
        builder.setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    private void xoaDialogHoaDon(){
        AlertDialog.Builder builder = new AlertDialog.Builder(getLayoutInflater().getContext());
        builder.setTitle("Xoa");
        builder.setMessage("Xoa The Loai?");
        builder.setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });
        builder.setPositiveButton("Xoa", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                hoaDonDao.deleteHoaDonById(dsHoaDon.get(adapterHoaDon.getPosition()).getMaHoaDon());
                try {
                    capNhatGiaoDienHoaDon();
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }
        });
        builder.show();
    }

    private void capNhatGiaoDienHoaDon() throws ParseException {
        dsHoaDon.clear();
        dsHoaDon = (ArrayList<HoaDon>) hoaDonDao.getAllHoaDon();
        adapterHoaDon.setListHoaDon(dsHoaDon);

    }

}

