package com.example.ps10349_mob204;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.example.ps10349_mob204.Dao.NguoiDungDao;
import com.example.ps10349_mob204.databinding.LoginBinding;

public class Login extends AppCompatActivity {
    private LoginBinding binding;
    NguoiDungDao nguoiDungDao;
    String strUser, strPass;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.login);
        nguoiDungDao = new NguoiDungDao(Login.this);
        binding.btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                checkLogin();
            }
        });
        binding.btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                binding.edUserName.setText("");
                binding.edPassword.setText("");
            }
        });


    }

    public void checkLogin() {
        strUser = binding.edUserName.getText().toString();
        strPass = binding.edPassword.getText().toString();
        if (strUser.isEmpty() || strPass.isEmpty()) {
            Toast.makeText(this, "Ten dang nhap va Mat khau khong duoc trong", Toast.LENGTH_SHORT).show();
        } else {
            if (nguoiDungDao.checkLogin(strUser, strPass) > 0) {
                Toast.makeText(this, "Dang nhap thanh cong", Toast.LENGTH_SHORT).show();
            }
            if (strUser.equalsIgnoreCase("admin") && strPass.equalsIgnoreCase("admin")) {
                rememberUser(strUser, strPass, binding.chkRememberPass.isChecked());
                Intent intent = new Intent(Login.this, MainNavigationDrawer.class);
                startActivity(intent);
                Toast.makeText(this, "Dang nhap thanh cong", Toast.LENGTH_SHORT).show();
                finish();
            } else {
                Toast.makeText(this, "Ten dang nhap hoac mat khau khong dung", Toast.LENGTH_SHORT).show();
            }
        }
    }

    public void rememberUser(String u, String p, boolean status) {
        SharedPreferences preferences = getSharedPreferences("USERFILE", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        if (!status) {
            //xoa tinh trang luu tru truoc do
            editor.clear();
        } else {
            //luu du lieu
            editor.putString("USERNAME", u);
            editor.putString("PASSWORD", p);
            editor.putBoolean("REMEMBER", status);
        }
        //luu lai toan bo
        editor.commit();
    }
}
