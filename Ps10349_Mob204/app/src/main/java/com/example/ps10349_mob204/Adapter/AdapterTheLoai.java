package com.example.ps10349_mob204.Adapter;

import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.ps10349_mob204.Model.TheLoai;
import com.example.ps10349_mob204.R;

import java.util.ArrayList;
import java.util.List;

public class AdapterTheLoai extends RecyclerView.Adapter<AdapterTheLoai.ViewHolder> {
    private List<TheLoai> listTheLoai;
    private int position = -1;

    public AdapterTheLoai(List<TheLoai> listNguoiDung) {
        this.listTheLoai = listNguoiDung;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View contactView = layoutInflater.inflate(R.layout.item_theloai, parent, false);
        return new ViewHolder(contactView);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {
        final TheLoai theLoai = listTheLoai.get(position);
        holder.tvTenTl.setText(theLoai.getTenTheLoai());
        holder.tvMaTl.setText(theLoai.getMaTheLoai());
        holder.itemView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                setPosition(holder.getAdapterPosition());
                return false;
            }
        });

    }

    @Override
    public int getItemCount() {
        return listTheLoai.size();
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public class ViewHolder extends RecyclerView.ViewHolder
            implements View.OnCreateContextMenuListener {
        TextView tvTenTl, tvMaTl;


        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            tvTenTl = itemView.findViewById(R.id.tv_ten_theloai);
            tvMaTl = itemView.findViewById(R.id.tv_ma_the_loai);
            itemView.setOnCreateContextMenuListener(this);
        }


        @Override
        public void onCreateContextMenu(ContextMenu contextMenu, View view,
                                        ContextMenu.ContextMenuInfo contextMenuInfo) {
            contextMenu.add(Menu.NONE, R.id.edit_the_loai,
                    Menu.NONE, R.string.edit);
            contextMenu.add(Menu.NONE, R.id.delete_the_loai,
                    Menu.NONE, R.string.delete);

        }
    }

    public void setListTheLoai(List<TheLoai> listTheLoai) {
        if (listTheLoai != null) {
            this.listTheLoai = new ArrayList<>(listTheLoai);
            notifyDataSetChanged();
        }
    }
}