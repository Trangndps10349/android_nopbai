package com.example.ps10349_mob204.Dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.example.ps10349_mob204.Database.DbHelper;
import com.example.ps10349_mob204.Model.HoaDon;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

public class HoaDonDao {
    private SQLiteDatabase db;
    private DbHelper dbHelper;
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");

    public HoaDonDao(Context context) {
        dbHelper = new DbHelper(context);
        db = dbHelper.getWritableDatabase();
    }



    //insert
    public int inserHoaDon(HoaDon hoaDon) {
        ContentValues values = new ContentValues();
        values.put("mahoadon", hoaDon.getMaHoaDon());
        values.put("ngaymua",simpleDateFormat.format(hoaDon.getNgayMuaHang()));
        try {
            if (db.insert("HoaDonTb", null, values) == -1) {
                return -1;
            }
        } catch (Exception ex) {
            Log.e("HoaDonTb", ex.toString());
        }
        return 1;
    }
    // getAll

    public List<HoaDon> getAllHoaDon() throws ParseException {
        List<HoaDon> dsHoaDon = new ArrayList<>();
        Cursor c = db.query("HoaDonTb", null, null, null, null, null, null);
        c.moveToFirst();
        while (c.isAfterLast() == false) {
            HoaDon hoaDon = new HoaDon();
            hoaDon.setMaHoaDon(c.getString(0));
            hoaDon.setNgayMuaHang(simpleDateFormat.parse(c.getString(1)));
            dsHoaDon.add(hoaDon);
            Log.d("===", hoaDon.toString());
            c.moveToNext();

        }
        c.close();
        return dsHoaDon;
    }

    //update
    public int updateHoaDon(HoaDon hoaDon) {
        ContentValues values = new ContentValues();
        values.put("mahoadon", hoaDon.getMaHoaDon());
        values.put("ngaymua", (hoaDon.getNgayMuaHang().toString()));
        int result = db.update("HoaDonTb", values, "mahoadon=?", new String[]{hoaDon.getMaHoaDon()});
        if (result == 0) {
            return -1;
        }
        return 1;
    }

    //delete
    public int deleteHoaDonById(String maHoaDon) {
        db= dbHelper.getWritableDatabase();
        int result = db.delete("HoaDonTb", "mahoadon=?", new String[]{maHoaDon});
        if (result == 0) {
            return -1;
        }
        return 1;
    }

}
