package com.example.asm_ps10349.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.asm_ps10349.Database.MonHocDao;
import com.example.asm_ps10349.Model.MonHoc;
import com.example.asm_ps10349.R;

import java.util.ArrayList;

public class AdapterMonHoc extends BaseAdapter {
    Context context;
    ArrayList<MonHoc> dsMonHoc;

    public AdapterMonHoc(Context context, ArrayList<MonHoc> dsMonHoc, MonHocListener listener) {
        this.context = context;
        this.dsMonHoc = dsMonHoc;
        this.listener = listener;
    }

    public interface MonHocListener{
        void xoaMonHoc(String id);
        void suaMonHoc(MonHoc monHoc);
    }
    MonHocListener listener;




    @Override
    public int getCount() {
        return dsMonHoc.size();
    }

    @Override
    public Object getItem(int position) {
        return dsMonHoc.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        if (convertView == null){
            // ket noi custom temview
            LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = layoutInflater.inflate(R.layout.item_mon_hoc,null);
            viewHolder = new ViewHolder();
            viewHolder.tvMaMon=  convertView.findViewById(R.id.tv_item_id_mon);
            viewHolder.tvTenMon= convertView.findViewById(R.id.tv_item_ten_mon);
            viewHolder.tvLichHoc= convertView.findViewById(R.id.tv_item_lich_hoc);
            viewHolder.imgSua= convertView.findViewById(R.id.img_edit_mon);
            viewHolder.imgXoa= convertView.findViewById(R.id.img_delete_mon);
            convertView.setTag(viewHolder);
        }else{
            viewHolder = (ViewHolder) convertView.getTag();
        }
        final MonHoc monHoc = dsMonHoc.get(position);

        viewHolder.tvMaMon.setText("Id mon hoc: "+monHoc.getId());
        viewHolder.tvTenMon.setText("Ten mon hoc: "+monHoc.getTenMonHoc());
        viewHolder.tvLichHoc.setText("Lich hoc: "+monHoc.getLichHoc());

        viewHolder.imgSua.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                    listener.suaMonHoc(monHoc);
            }
        });
        viewHolder.imgXoa.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.xoaMonHoc(dsMonHoc.get(position).getId());
            }
        });
        return convertView;
    }
    class ViewHolder{
    ImageView imgSua,imgXoa;
    TextView tvMaMon,tvTenMon,tvLichHoc;
    public  ViewHolder(){}
    }

}
