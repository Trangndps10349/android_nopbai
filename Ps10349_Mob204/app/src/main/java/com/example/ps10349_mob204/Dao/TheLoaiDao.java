package com.example.ps10349_mob204.Dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.example.ps10349_mob204.Database.DbHelper;
import com.example.ps10349_mob204.Model.TheLoai;

import java.util.ArrayList;
import java.util.List;

public class TheLoaiDao {
    private SQLiteDatabase db;
    private DbHelper dbHelper;

    public TheLoaiDao(Context context) {
        dbHelper = new DbHelper(context);
        db = dbHelper.getWritableDatabase();
    }

    //insert
    public int inserTheLoai(TheLoai theLoai) {
        ContentValues values = new ContentValues();
        values.put("matheloai", theLoai.getMaTheLoai());
        values.put("tentheloai", theLoai.getTenTheLoai());
        values.put("mota", theLoai.getMoTa());
        values.put("vitri", theLoai.getViTri());
        try {
            if (db.insert("TheLoaiTb", null, values) == -1) {
                return -1;
            }
        } catch (Exception ex) {
            Log.e("TheLoaiTb", ex.toString());
        }
        return 1;
    }

    //getAllTheLoai
    public List<TheLoai> getAllTheLoai() {
        List<TheLoai> dsTheLoai = new ArrayList<>();
        Cursor c = db.query("TheLoaiTb", null, null, null, null, null, null);
        c.moveToFirst();
        while (c.isAfterLast() == false) {
            TheLoai theLoai = new TheLoai();
            theLoai.setMaTheLoai(c.getString(0));
            theLoai.setTenTheLoai(c.getString(1));
            theLoai.setMoTa(c.getString(2));
            theLoai.setViTri(c.getInt(3));
            dsTheLoai.add(theLoai);
            Log.d("===", theLoai.toString());
            c.moveToNext();

        }
        c.close();
        return dsTheLoai;
    }

    //update the loai
    public int updateTheloai(TheLoai theLoai) {
        ContentValues values = new ContentValues();
        values.put("matheloai", theLoai.getMaTheLoai());
        values.put("tentheloai", theLoai.getTenTheLoai());
        values.put("mota", theLoai.getMoTa());
        values.put("vitri", theLoai.getViTri());
        int result = db.update("TheLoaiTb", values, "matheloai=?", new String[]{theLoai.getMaTheLoai()});
        if (result == 0) {
            return -1;
        }
        return 1;
    }

    //delete
    public int deleteTheLoaiById(String maTheLoai) {
        int result = db.delete("TheLoaiTb", "matheloai=?", new String[]{maTheLoai});
        if (result == 0) {
            return -1;
        }
        return 1;
    }

    public void deleteTheLoai(TheLoai theLoai) {
        db = dbHelper.getWritableDatabase();
        db.beginTransaction();
        try {

            db.delete("TheLoaiTb", "matheloai=?", new String[]{theLoai.getMaTheLoai() + ""});
            db.setTransactionSuccessful();
        } catch (Exception e) {
        } finally {
            db.endTransaction();
        }
    }
}
