package com.example.ps10349_mob204.Model;

public class Sach {
    private String maSach;
    private String maTheLoai;
    private String tieuDeSach;
    private String tacGia;
    private Integer soLuong;
    private Double giaBia;
    private String nhaXuatBan;

    public Sach(String maSach, String maTheLoai, String tieuDeSach, String tacGia, Integer soLuong, Double giaBia, String nhaXuatBan) {
        this.maSach = maSach;
        this.maTheLoai = maTheLoai;
        this.tieuDeSach = tieuDeSach;
        this.tacGia = tacGia;
        this.soLuong = soLuong;
        this.giaBia = giaBia;
        this.nhaXuatBan = nhaXuatBan;
    }

    public Sach() {
    }


    public String getMaSach() {
        return maSach;
    }

    public void setMaSach(String maSach) {
        this.maSach = maSach;
    }

    public String getMaTheLoai() {
        return maTheLoai;
    }

    public void setMaTheLoai(String maTheLoai) {
        this.maTheLoai = maTheLoai;
    }

    public String getTieuDeSach() {
        return tieuDeSach;
    }

    public void setTieuDeSach(String tieuDeSach) {
        this.tieuDeSach = tieuDeSach;
    }

    public String getTacGia() {
        return tacGia;
    }

    public void setTacGia(String tacGia) {
        this.tacGia = tacGia;
    }

    public Integer getSoLuong() {
        return soLuong;
    }

    public void setSoLuong(Integer soLuong) {
        this.soLuong = soLuong;
    }

    public Double getGiaBia() {
        return giaBia;
    }

    public void setGiaBia(Double giaBia) {
        this.giaBia = giaBia;
    }

    public String getNhaXuatBan() {
        return nhaXuatBan;
    }

    public void setNhaXuatBan(String nhaXuatBan) {
        this.nhaXuatBan = nhaXuatBan;
    }

    @Override
    public String toString() {
        return "Sach{" +
                "maSach='" + maSach + '\'' +
                ", maTheLoai='" + maTheLoai + '\'' +
                ", tieuDeSach='" + tieuDeSach + '\'' +
                ", tacGia='" + tacGia + '\'' +
                ", soLuong=" + soLuong +
                ", giaBia=" + giaBia +
                ", nhaXuatBan='" + nhaXuatBan + '\'' +
                '}';
    }
}
