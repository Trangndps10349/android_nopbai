package com.example.asm_ps10349.Model;

public class MonHoc {
    private String id;
    private String TenMonHoc;
    private String LichHoc;

    public MonHoc() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTenMonHoc() {
        return TenMonHoc;
    }

    public void setTenMonHoc(String tenMonHoc) {
        TenMonHoc = tenMonHoc;
    }

    public String getLichHoc() {
        return LichHoc;
    }

    public void setLichHoc(String lichHoc) {
        LichHoc = lichHoc;
    }

    public MonHoc(String id, String tenMonHoc, String lichHoc) {
        this.id = id;
        TenMonHoc = tenMonHoc;
        LichHoc = lichHoc;
    }

    @Override
    public String toString() {
        return "MonHoc{" +
                "id='" + id + '\'' +
                ", TenMonHoc='" + TenMonHoc + '\'' +
                ", LichHoc='" + LichHoc + '\'' +
                '}';
    }
}
