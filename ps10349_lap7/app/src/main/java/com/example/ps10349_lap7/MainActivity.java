package com.example.ps10349_lap7;

import androidx.appcompat.app.AppCompatActivity;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    ImageView imageView;
    Button btnRotation, btnMoving, btnZoom, btnAll, btnDoraemon, btnNobita;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        imageView = findViewById(R.id.imageView);
        btnRotation = findViewById(R.id.button);
        btnMoving = findViewById(R.id.button2);
        btnZoom = findViewById(R.id.button3);
        btnAll = findViewById(R.id.button_all);
        btnDoraemon = findViewById(R.id.button_doraemon);
        btnNobita = findViewById(R.id.boton_nobita);


        btnRotation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ObjectAnimator animation1 = ObjectAnimator.ofFloat(imageView, "rotation", 360);
                animation1.setDuration(2000);
                animation1.start();

            }
        });
        //zoom
        btnZoom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Animation animation = AnimationUtils.loadAnimation(MainActivity.this, R.anim.zoom);
                imageView.startAnimation(animation);
            }
        });
        //moving
        btnMoving.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ObjectAnimator animationX = ObjectAnimator.ofFloat(imageView, "translationX", -360f, 0f);
                animationX.setDuration(5000);

                ObjectAnimator animationY = ObjectAnimator.ofFloat(imageView, "translationX", 0f, -360f);
                animationY.setDuration(5000);


                AnimatorSet animatorSet = new AnimatorSet();
                animatorSet.play(animationX).after(animationY);
                animatorSet.start();
            }
        });
        btnAll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showImage("All");
            }
        });
        btnDoraemon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showImage("Doraemon");
            }
        });
        btnNobita.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showImage("Nobita");
            }
        });
    }

    private void showImage(final String img) {
        //hide img
        ObjectAnimator animator = ObjectAnimator.ofFloat(imageView, "translationX", 0f, 50f);
        animator.setDuration(2000);
        ObjectAnimator animator1 = ObjectAnimator.ofFloat(imageView, "alpha", 1f, 0f);
        animator1.setDuration(2000);
        //show img
        ObjectAnimator animator2 = ObjectAnimator.ofFloat(imageView, "translationX", -500f, 0f);
        animator2.setDuration(2000);
        ObjectAnimator animator3 = ObjectAnimator.ofFloat(imageView, "alpha", 0f, 1f);
        animator3.setDuration(2000);

        AnimatorSet animatorSet = new AnimatorSet();
        animatorSet.play(animator2).with(animator3).after(animator1);
        animatorSet.start();
        final String imgName = img;
        animator1.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationCancel(Animator animation) {
                super.onAnimationCancel(animation);
            }

            @Override
            public void onAnimationEnd(Animator animation) {
                super.onAnimationEnd(animation);
                if (imgName == "Nobita") {
                    imageView.setImageResource(R.drawable.nobita);
                }
                if (imgName == "Doraemon") {
                    imageView.setImageResource(R.drawable.doraemon);
                }
            }

            @Override
            public void onAnimationStart(Animator animation) {
                super.onAnimationStart(animation);

            }
        });


    }
}
