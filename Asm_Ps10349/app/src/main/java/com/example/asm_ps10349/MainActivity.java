package com.example.asm_ps10349;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class MainActivity extends AppCompatActivity {
    ImageButton imbCoure,imbMaps,imbNews,imbSocial;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        anhxa();

        imbCoure.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this,SourseActivity.class);
                startActivity(intent);
            }
        });
        imbMaps.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
            Intent intent = new Intent(MainActivity.this,MapsActivity.class);
            startActivity(intent);
            }
        });
        imbNews.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this,NewsActivity.class);
                startActivity(intent);
            }
        });

        imbSocial.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                    Intent intent = new Intent(MainActivity.this,SocialActivity.class);
                    startActivity(intent);
            }
        });
    }
    private void anhxa(){
        imbCoure = findViewById(R.id.imb_coure);
        imbMaps = findViewById(R.id.imb_maps);
        imbNews = findViewById(R.id.imb_news);
        imbSocial = findViewById(R.id.imb_social);
    }
}
