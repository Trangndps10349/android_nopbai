package com.example.ps10349_mob204.Fragment;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.SearchView;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.ps10349_mob204.Adapter.AdapterSach;
import com.example.ps10349_mob204.Adapter.SpAdapter;
import com.example.ps10349_mob204.Dao.SachDao;
import com.example.ps10349_mob204.Dao.TheLoaiDao;
import com.example.ps10349_mob204.Model.Sach;
import com.example.ps10349_mob204.Model.TheLoai;
import com.example.ps10349_mob204.R;

import java.util.ArrayList;
import java.util.List;

public class ChildFragmentChiTietSach extends Fragment {
    AdapterSach adapterSach;
    RecyclerView sachRecycleview;

    SachDao sachDao;
    TheLoaiDao theLoaiDao;
    ArrayList<Sach> listSach;
    ArrayList<TheLoai> danhSachTheLoai;
    SpAdapter spAdapter;




    public static ChildFragmentChiTietSach newInstance() {
        Bundle args = new Bundle();
        ChildFragmentChiTietSach fragment = new ChildFragmentChiTietSach();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.child_fragment_sach_chitiet, container, false);
        listSach = new ArrayList<>();
        danhSachTheLoai = new ArrayList<>();

        sachDao = new SachDao(getContext());
        theLoaiDao = new TheLoaiDao(getContext());

        danhSachTheLoai = (ArrayList<TheLoai>) theLoaiDao.getAllTheLoai();

        sachRecycleview = v.findViewById(R.id.recyclerview_chitiet_sach);

        spAdapter = new SpAdapter(getContext(),danhSachTheLoai);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext(),LinearLayoutManager.HORIZONTAL,false);
        sachRecycleview.setLayoutManager(linearLayoutManager);
        adapterSach = new AdapterSach(listSach);
        sachRecycleview.setAdapter(adapterSach);
        capNhatGiaoDienSach();
        registerForContextMenu(sachRecycleview);
        setHasOptionsMenu(true);
        return v;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        view.findViewById(R.id.fab_them_sach).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ThemSachDialog();
            }
        });
    }

    @Override
    public boolean onContextItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.edit_sach:
                if (listSach != null
                        && listSach.size() > adapterSach.getPosition()
                        && adapterSach.getPosition() >= 0) {
                    SuaSachDialog(listSach.get(adapterSach.getPosition()));
                }
                return true;

            case R.id.delete_sach:
                if (listSach != null
                        && listSach.size() > adapterSach.getPosition()
                        && adapterSach.getPosition() >= 0) {
                    XoaSachDialog();
                }
                return true;
        }
        return super.onContextItemSelected(item);
    }
    //////////tim sach
    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        inflater.inflate(R.menu.search_sach_menu,menu);
        MenuItem searchItem = menu.findItem(R.id.search_sach);
        SearchView searchView = (SearchView)searchItem.getActionView();
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                    adapterSach.getFilter().filter(s);
                return false;
            }
        });
    }

    private void ThemSachDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getLayoutInflater().getContext());

        final LayoutInflater inflater = getActivity().getLayoutInflater();
        View v = inflater.inflate(R.layout.dialog_them_sach, null);

        final EditText edtTieuDe = v.findViewById(R.id.edt_tieu_de);
        final Spinner spMaTL = v.findViewById(R.id.sp_ma_tl);
        final EditText edtMaSach = v.findViewById(R.id.edt_them_ma_sach);
        final EditText edtTacGia = v.findViewById(R.id.edt_them_tac_gia);
        final EditText edtNXB = v.findViewById(R.id.edt_them_nxb);
        final EditText edtGiaBia = v.findViewById(R.id.edt_them_gia);
        final EditText edtSoLuong = v.findViewById(R.id.edt_sl);

        spMaTL.setAdapter(spAdapter);

        builder.setTitle("THEM SACH");
        builder.setView(v);

        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                Sach  sach = new Sach();
                 String txtTieuDe = edtTieuDe.getText().toString();
                 String txtMaSach = edtMaSach.getText().toString();
                 String txtTacGia = edtTacGia.getText().toString();
                 String txtNXB = edtNXB.getText().toString();
                 String txtGiaBia = edtGiaBia.getText().toString();
                 String txtSoLuong = edtSoLuong.getText().toString();
                 TheLoai theLoai = (TheLoai) spMaTL.getSelectedItem();
                 String txtMaTL = theLoai.getMaTheLoai();

                if (TextUtils.isEmpty(txtTieuDe)) {
                    Toast.makeText(getContext(), "Ten tieu de trong", Toast.LENGTH_SHORT).show();
                }if (TextUtils.isEmpty(txtMaSach)) {
                    Toast.makeText(getContext(), "Ma sach trong", Toast.LENGTH_SHORT).show();
                }if (TextUtils.isEmpty(txtTacGia)) {
                    Toast.makeText(getContext(), "Ten tac gia trong", Toast.LENGTH_SHORT).show();
                }if (TextUtils.isEmpty(txtNXB)) {
                    Toast.makeText(getContext(), "Ten tieu de trong", Toast.LENGTH_SHORT).show();
                }if (TextUtils.isEmpty(txtGiaBia)) {
                    Toast.makeText(getContext(), "Gia bia trong", Toast.LENGTH_SHORT).show();
                }if (TextUtils.isEmpty(txtSoLuong)) {
                    Toast.makeText(getContext(), "Nhap so luong ", Toast.LENGTH_SHORT).show();
                }else {
                    sach.setTieuDeSach(txtTieuDe);
                    sach.setMaSach(txtMaSach);
                    sach.setTacGia(txtTacGia);
                    sach.setNhaXuatBan(txtNXB);
                    sach.setGiaBia(Double.valueOf(txtGiaBia));
                    sach.setSoLuong(Integer.valueOf(txtSoLuong));
                    sach.setMaTheLoai(txtMaTL);

                    if (sachDao.insertSach(sach)==-1){
                        Toast.makeText(getContext(), "Them ko thanh cong", Toast.LENGTH_SHORT).show();
                    }else{
                        listSach.add(sach);
                        capNhatGiaoDienSach();
                        Toast.makeText(getContext(), "Them thanh cong", Toast.LENGTH_SHORT).show();
                    }
                }

            }
        });
        builder.setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    private void SuaSachDialog(Sach sach) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getLayoutInflater().getContext());
        final LayoutInflater inflater = getActivity().getLayoutInflater();

        View v = inflater.inflate(R.layout.dialog_sua_sach, null);
        final EditText edtTieuDe = v.findViewById(R.id.edt_sua_tieu_de);
        final Spinner spMaTL = v.findViewById(R.id.sp_ma_tl);
        final EditText edtMaSach = v.findViewById(R.id.edt_sua_ma_sach);
        final EditText edtTacGia = v.findViewById(R.id.edt_sua_tac_gia);
        final EditText edtNXB = v.findViewById(R.id.edt_sua_nxb);
        final EditText edtGiaBia = v.findViewById(R.id.edt_sua_gia);
        final EditText edtSoLuong = v.findViewById(R.id.edt_sua_sl);
        spMaTL.setAdapter(spAdapter);
        if (sach != null){
            edtTieuDe.setText(sach.getTieuDeSach());
            edtMaSach.setText(sach.getMaSach());
            edtTacGia.setText(sach.getTacGia());
            spMaTL.getSelectedItem().toString();
            edtNXB.setText(sach.getNhaXuatBan());
            edtGiaBia.setText(String.valueOf(sach.getGiaBia()));
            edtSoLuong.setText(String.valueOf(sach.getSoLuong()));
        }else {
            edtTieuDe.setText("");
            edtMaSach.setText("");
            edtNXB.setText("");
            edtTacGia.setText("");
            edtSoLuong.setText("");
            edtGiaBia.setText("");
        }
        builder.setTitle("SUA SACH");
        builder.setView(v);
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                Sach  sach = new Sach();
                String txtTieuDe = edtTieuDe.getText().toString();
                String txtMaSach = edtMaSach.getText().toString();
                String txtTacGia = edtTacGia.getText().toString();
                String txtNXB = edtNXB.getText().toString();
                String txtGiaBia = edtGiaBia.getText().toString();
                String txtSoLuong = edtSoLuong.getText().toString();
                TheLoai theLoai = (TheLoai) spMaTL.getSelectedItem();
                String txtMaTL = theLoai.getMaTheLoai();

                if (TextUtils.isEmpty(txtTieuDe)) {
                    Toast.makeText(getContext(), "Ten tieu de trong", Toast.LENGTH_SHORT).show();
                }if (TextUtils.isEmpty(txtMaSach)) {
                    Toast.makeText(getContext(), "Ma sach trong", Toast.LENGTH_SHORT).show();
                }if (TextUtils.isEmpty(txtTacGia)) {
                    Toast.makeText(getContext(), "Ten tac gia trong", Toast.LENGTH_SHORT).show();
                }if (TextUtils.isEmpty(txtNXB)) {
                    Toast.makeText(getContext(), "Ten tieu de trong", Toast.LENGTH_SHORT).show();
                }if (TextUtils.isEmpty(txtGiaBia)) {
                    Toast.makeText(getContext(), "Gia bia trong", Toast.LENGTH_SHORT).show();
                }if (TextUtils.isEmpty(txtSoLuong)) {
                    Toast.makeText(getContext(), "Nhap so luong ", Toast.LENGTH_SHORT).show();
                }else {
                    sach.setTieuDeSach(txtTieuDe);
                    sach.setMaSach(txtMaSach);
                    sach.setTacGia(txtTacGia);
                    sach.setNhaXuatBan(txtNXB);
                    sach.setGiaBia(Double.valueOf(txtGiaBia));
                    sach.setSoLuong(Integer.valueOf(txtSoLuong));
                    sach.setMaTheLoai(txtMaTL);

                    if (sachDao.updateSach(sach)==-1){
                        Toast.makeText(getContext(), "Sua ko thanh cong", Toast.LENGTH_SHORT).show();
                    }else{
                        listSach.add(sach);
                        capNhatGiaoDienSach();
                        Toast.makeText(getContext(), "Sua thanh cong", Toast.LENGTH_SHORT).show();
                    }
                }

            }
        });
        builder.setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    private void XoaSachDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getLayoutInflater().getContext());
        builder.setTitle("Xoa");
        builder.setMessage("Xoa Sach?");
        builder.setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });
        builder.setPositiveButton("Xoa", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                sachDao.deleteSachById(listSach.get(adapterSach.getPosition()).getMaSach());
                capNhatGiaoDienSach();
            }
        });
        builder.show();
    }



    private void capNhatGiaoDienSach() {
        listSach.clear();
        listSach = (ArrayList<Sach>) sachDao.getAllSach();
        adapterSach.setListSach(listSach);
    }


}
