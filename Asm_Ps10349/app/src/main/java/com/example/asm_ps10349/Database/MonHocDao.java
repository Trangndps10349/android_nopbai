package com.example.asm_ps10349.Database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.example.asm_ps10349.Model.MonHoc;

import java.util.ArrayList;
import java.util.List;

public class MonHocDao {
    SQLiteDatabase db;
    DbHelper dbHelper;

    public MonHocDao(Context context) {
        dbHelper = new DbHelper(context);
    }

    public int ThemMonHoc(MonHoc monHoc) {
        db = dbHelper.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("idmonhoc", monHoc.getId());
        contentValues.put("tenmonhoc", monHoc.getTenMonHoc());
        contentValues.put("lichhoc", monHoc.getLichHoc());
        try {
            if (db.insert("monhoc", null, contentValues) == -1) {
                return -1;
            }
        } catch (Exception ex) {
            Log.e("MonHoc", ex.toString());
        }
        return 1;
    }


    public List<MonHoc> getAllMonHoc() {
        ArrayList<MonHoc> dsMonHoc = new ArrayList<>();
        db = dbHelper.getReadableDatabase();
        Cursor c = db.query("monhoc", null, null, null, null, null, null);
        c.moveToFirst();
        while (c.isAfterLast() == false) {
            MonHoc monHoc = new MonHoc();
            monHoc.setId(c.getString(0));
            monHoc.setTenMonHoc(c.getString(1));
            monHoc.setLichHoc(c.getString(2));
            dsMonHoc.add(monHoc);
            c.moveToNext();
        }
        c.close();
        return dsMonHoc;
    }

    public int suaMonHoc(MonHoc monHoc) {
        db= dbHelper.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("idmonhoc", monHoc.getId());
        values.put("tenmonhoc", monHoc.getTenMonHoc());
        values.put("lichhoc", monHoc.getLichHoc());
        int result = db.update("monhoc", values, "idmonhoc=?", new String[]{monHoc.getId()});
        if (result == 0) {
            return -1;
        }
        return 1;
    }
    public int xoaMonHoc(String idMonHoc){
        db = dbHelper.getWritableDatabase();
        int result = db.delete("monhoc","idmonhoc = ?",new String[]{idMonHoc});
        if (result == 0){
            return -1;
        }
        return 1;
    }


}
