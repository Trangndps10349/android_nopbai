package com.example.asm_ps10349.Database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.example.asm_ps10349.Model.LichThi;
import com.example.asm_ps10349.Model.MonHoc;

import java.util.ArrayList;
import java.util.List;

public class LichThiDao {
    SQLiteDatabase db;
    DbHelper dbHelper;

    public LichThiDao(Context context) {
        dbHelper = new DbHelper(context);
    }

    public int themLichThi(LichThi lichThi) {
        db = dbHelper.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("idlichthi", lichThi.getMaLichThi());
        contentValues.put("tenlichthi", lichThi.getTenMonThi());
        contentValues.put("ngaythi", lichThi.getNgayThi());
        contentValues.put("mamon",lichThi.getMaMonThi());
        try {
            if (db.insert("lichthi", null, contentValues) == -1) {
                return -1;
            }
        } catch (Exception ex) {
            Log.e("lichthi", ex.toString());
        }
        return 1;
    }


    public List<LichThi> getAllLichThi() {
        ArrayList<LichThi> dsLichThi = new ArrayList<>();
        db = dbHelper.getReadableDatabase();
        Cursor c = db.query("lichthi", null, null, null, null, null, null);
        c.moveToFirst();
        while (c.isAfterLast() == false) {
            LichThi lichThi = new LichThi();
            lichThi.setMaLichThi(c.getString(0));
            lichThi.setTenMonThi(c.getString(1));
            lichThi.setNgayThi(c.getString(2));
            lichThi.setMaMonThi(c.getString(3));
            dsLichThi.add(lichThi);
            c.moveToNext();
        }
        c.close();
        return dsLichThi;
    }
    public List<LichThi> timmonthi() {
        ArrayList<LichThi> dsLichThi = new ArrayList<>();
        db = dbHelper.getReadableDatabase();
        Cursor c = db.rawQuery("select *")
        c.moveToFirst();
        while (c.isAfterLast() == false) {
            LichThi lichThi = new LichThi();
            lichThi.setMaLichThi(c.getString(0));
            lichThi.setTenMonThi(c.getString(1));
            lichThi.setNgayThi(c.getString(2));
            lichThi.setMaMonThi(c.getString(3));
            dsLichThi.add(lichThi);
            c.moveToNext();
        }
        c.close();
        return dsLichThi;
    }

    public int suaLichThi(LichThi lichThi) {
        db = dbHelper.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("idlichthi", lichThi.getMaLichThi());
        contentValues.put("tenlichthi", lichThi.getTenMonThi());
        contentValues.put("ngaythi", lichThi.getNgayThi());
        contentValues.put("mamon",lichThi.getMaMonThi());
        int result = db.update("lichthi", contentValues, "idlichthi", new String[]{lichThi.getMaLichThi()});
        if (result == 0) {
            return -1;
        }
        return 1;
    }
    public int xoaLichthi(String maMonHoc){
        db = dbHelper.getWritableDatabase();
        int result = db.delete("lichthi","idlichthi",new String[]{maMonHoc});
        if (result == 0){
            return -1;
        }
        return 1;
    }


}
