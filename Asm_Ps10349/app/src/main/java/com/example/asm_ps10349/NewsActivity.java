package com.example.asm_ps10349;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;

import com.example.asm_ps10349.Adapter.AdapterListNews;
import com.example.asm_ps10349.Model.News;
import com.example.asm_ps10349.Model.NewsReader;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;

import static com.example.asm_ps10349.Webview.LINK_ARGS;

public class NewsActivity extends AppCompatActivity implements AdapterListNews.Listener {
    private RecyclerView recyclerView;
    private AdapterListNews adapterListNews;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_news);
        recyclerView = findViewById(R.id.list_news);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        adapterListNews = new AdapterListNews(this);
        recyclerView.setAdapter(adapterListNews);

        NewsSeedAsyncTask newsSeedAsyncTask = new NewsSeedAsyncTask();
        newsSeedAsyncTask.execute();
    }

    @Override
    public void onClickNews(News news) {
        Intent intent = new Intent(this, Webview.class);
        intent.putExtra(LINK_ARGS, news.getLink());
        startActivity(intent);
    }

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }


    class NewsSeedAsyncTask extends AsyncTask<Void, Void, List<News>> {

        @Override
        protected List<News> doInBackground(Void... voids) {
            try {
                URL url = new URL("https://vnexpress.net/rss/giao-duc.rss");
                HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
                InputStream inputStream = httpURLConnection.getInputStream();
                return NewsReader.listNews(inputStream);
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(List<News> news) {
            super.onPostExecute(news);
            if (news != null) {
                adapterListNews.setListNews(news);
            }
        }
    }
}
