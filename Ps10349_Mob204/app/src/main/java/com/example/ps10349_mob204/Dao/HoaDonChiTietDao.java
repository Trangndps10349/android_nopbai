package com.example.ps10349_mob204.Dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.example.ps10349_mob204.Database.DbHelper;
import com.example.ps10349_mob204.Model.HoaDon;
import com.example.ps10349_mob204.Model.HoaDonChiTiet;
import com.example.ps10349_mob204.Model.Sach;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

public class HoaDonChiTietDao {

    private SQLiteDatabase db;
    private DbHelper dbHelper;

    public HoaDonChiTietDao(Context context) {
        dbHelper = new DbHelper(context);
        db = dbHelper.getWritableDatabase();
    }

    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

    //insert
    public int HoaDonChiTiet(HoaDonChiTiet hdct) {
        ContentValues values = new ContentValues();
        values.put("mahoadon", hdct.getHoaDon().getMaHoaDon());
        values.put("masach", hdct.getSach().getMaSach());
        values.put("soluong", hdct.getSoLuongMua());
        try {
            if (db.insert("HoaDonChiTietTb", null, values) == -1) {
                return -1;
            }
        } catch (Exception ex) {
            Log.e("HoaDonChiTietTb", ex.toString());
        }
        return 1;
    }

    //getAll
    public List<HoaDonChiTiet> getAllHoaDonChiTiet() {
        List<HoaDonChiTiet> dsHoaDonChiTiet = new ArrayList<>();
        String sQl = "SELECT mahdct,HoaDonTb.mahoadon,HoaDonTb.ngaymua," +
                "SachTb.masach,SachTb.matheloai,SachTb.tensach,SachTb.tacgia,SachTb.nxb,SachTb.giabia,SachTb.soluong," +
                "HoaDonChiTietTb.soluong " +
                "FROM HoaDonChiTietTb INNER JOIN HoaDonTb " +
                "on HoaDonChiTietTb.mahoadon=HoaDonTb.mahoadon INNER JOIN SachTb " +
                "on SachTb.masach = HoaDonChiTietTb.masach";
        Cursor c = db.rawQuery(sQl, null);
        c.moveToFirst();
        try {
            while (c.isAfterLast() == false) {
                HoaDonChiTiet hoaDonChiTiet = new HoaDonChiTiet();
                hoaDonChiTiet.setMaHDCT(c.getInt(0));
                hoaDonChiTiet.setHoaDon(new HoaDon(c.getString(1), sdf.parse(c.getString(2))));
                hoaDonChiTiet.setSach(new Sach(c.getString(3), c.getString(4), c.getString(5),
                        c.getString(6), c.getInt(7), c.getDouble(8), c.getString(9)));
                hoaDonChiTiet.setSoLuongMua(c.getInt(10));
                dsHoaDonChiTiet.add(hoaDonChiTiet);
                Log.d("//=====", hoaDonChiTiet.toString());
                c.moveToNext();
            }
            c.close();
        } catch (Exception ex) {
            Log.d("HoaDonChiTieiTb", ex.toString());
        }
        return dsHoaDonChiTiet;
    }

    //getAllById
    public List<HoaDonChiTiet> getAllHoaDonChiTietById(String maHoaDon) {
        List<HoaDonChiTiet> dsHoaDonChiTiet = new ArrayList<>();
        String sQl = "SELECT mahdct,HoaDonTb.mahoadon,HoaDonTb.ngaymua," +
                "SachTb.masach,SachTb.matheloai,SachTb.tensach,SachTb.tacgia,SachTb.nxb,SachTb.giabia,SachTb.soluong," +
                "HoaDonChiTietTb.soluong " +
                "FROM HoaDonChiTietTb INNER JOIN HoaDonTb " +
                "on HoaDonChiTietTb.mahoadon=HoaDonTb.mahoadon INNER JOIN SachTb " +
                "on SachTb.masach = HoaDonChiTietTb.masach WHERE HoaDonChiTietTB.mahoadon='" + maHoaDon + "'";
        Cursor c = db.rawQuery(sQl, null);
        c.moveToFirst();
        try {
            while (c.isAfterLast() == false) {
                HoaDonChiTiet hoaDonChiTiet = new HoaDonChiTiet();
                hoaDonChiTiet.setMaHDCT(c.getInt(0));
                hoaDonChiTiet.setHoaDon(new HoaDon(c.getString(1), sdf.parse(c.getString(2))));
                hoaDonChiTiet.setSach(new Sach(c.getString(3), c.getString(4), c.getString(5),
                        c.getString(6), c.getInt(7), c.getDouble(8), c.getString(9)));
                hoaDonChiTiet.setSoLuongMua(c.getInt(10));
                dsHoaDonChiTiet.add(hoaDonChiTiet);
                Log.d("//=====", hoaDonChiTiet.toString());
                c.moveToNext();
            }
            c.close();
        } catch (Exception ex) {
            Log.d("HoaDonChiTieiTb", ex.toString());
        }
        return dsHoaDonChiTiet;
    }

    //update
    public int updateHoaDonChiTiet(HoaDonChiTiet hdct) {
        ContentValues values = new ContentValues();
        values.put("mahdct", hdct.getMaHDCT());
        values.put("mahoadon", hdct.getHoaDon().getMaHoaDon());
        values.put("masach", hdct.getSach().getMaSach());
        values.put("soluong", hdct.getSoLuongMua());
        int result = db.update("HoaDonChiTietTb", values, "mahdct=?",
                new String[]{String.valueOf(hdct.getMaHDCT())});
        if (result == 0) {
            return -1;
        }
        return 1;
    }

    //delete
    public int deleteHoaDonChiTiet(String maHDCT) {
        db = dbHelper.getWritableDatabase();
        int result = db.delete("HoaDonChiTietTb", "mahdct=?", new String[]{maHDCT});
        if (result == 0) {
            return -1;
        }
        return 1;
    }

    //check
    public boolean checkHoaDonCT(String maHoaDon) {
        //SELECT
        String[] columns = {"mahoadon"};
        //WHERE clause
        String selection = "mahoadon=?";
        //WHERE clause arguments
        String[] selectionArgs = {maHoaDon};
        Cursor c = null;
        try {
            c = db.query("HoaDonChiTietTb", columns, selection, selectionArgs,
                    null, null, null);
            c.moveToFirst();
            int i = c.getCount();
            c.close();
            return i > 0;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    //doanh thu theo ngay
    public double getDoanhThuTheoNgay() {
        double doanhthu = 0;
        String sSQL = "SELECT SUM (tongtien) from (SELECT SUM(SachTb.giabia * HoaDonChiTietTb.soluong)" +
                "as 'tongtien'" +
                "FROM HoaDonTb INNER JOIN HoaDonChiTietTb on HoaDonTb.mahoadon = HoaDonChiTietTb.mahoadon " +
                "INNER JOIN SachTb on HoaDonChiTietTb.masach = SachTb.masach " +
                "WHERE HoaDonTb.ngaymua=date('now') GROUP BY HoaDonChiTietTb.masach)tmp";
        Cursor c = db.rawQuery(sSQL, null);
        c.moveToFirst();
        while (c.isAfterLast() == false) {
            doanhthu = c.getDouble(0);
            c.moveToNext();
        }
        c.close();
        return doanhthu;

    }

    //doanh thu theo thang
    public double getDoanhThuTheoThang() {
        double doanhthu = 0;
        String sSQL = "SELECT SUM (tongtien) from (SELECT SUM(SachTb.giabia * HoaDonChiTietTb.soluong)" +
                "as 'tongtien'" +
                "FROM HoaDonTb INNER JOIN HoaDonChiTietTb on HoaDonTb.mahoadon = HoaDonChiTietTb.mahoadon " +
                "INNER JOIN SachTb on HoaDonChiTietTb.masach = SachTb.masach " +
                "WHERE strftime('%m',HoaDonTb.ngaymua)=strftime('%m','now') GROUP BY HoaDonChiTietTb.masach)tmp";
        Cursor c = db.rawQuery(sSQL, null);
        c.moveToFirst();
        while (c.isAfterLast() == false) {
            doanhthu = c.getDouble(0);
            c.moveToNext();
        }
        c.close();
        return doanhthu;

    }

    //doanh thu theo nam
    public double getDoanhThuTheoNam() {
        double doanhthu = 0;
        String sSQL = "SELECT SUM (tongtien) from (SELECT SUM(SachTb.giabia * HoaDonChiTietTb.soluong)" +
                "as 'tongtien'" +
                "FROM HoaDonTb INNER JOIN HoaDonChiTietTb on HoaDonTb.mahoadon = HoaDonChiTietTb.mahoadon " +
                "INNER JOIN SachTb on HoaDonChiTietTb.masach = SachTb.masach " +
                "WHERE strftime('%Y',HoaDonTb.ngaymua)=strftime('%Y','now') GROUP BY HoaDonChiTietTb.masach)tmp";
        Cursor c = db.rawQuery(sSQL, null);
        c.moveToFirst();
        while (c.isAfterLast() == false) {
            doanhthu = c.getDouble(0);
            c.moveToNext();
        }
        c.close();
        return doanhthu;

    }

}

