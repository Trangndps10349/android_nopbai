package com.example.ps10349_mob204.Adapter;

import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.ps10349_mob204.Model.NguoiDung;
import com.example.ps10349_mob204.R;

import java.util.ArrayList;
import java.util.List;

public class AdapterNguoiDung extends RecyclerView.Adapter<AdapterNguoiDung.ViewHolder> {
    private List<NguoiDung> listNguoiDung;
    private List<NguoiDung> listNguoiDungSort;
    private int position = -1;

    public AdapterNguoiDung(List<NguoiDung> listNguoiDung) {
        this.listNguoiDung = listNguoiDung;
        listNguoiDungSort = new ArrayList<>(listNguoiDung);
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View contactView = layoutInflater.inflate(R.layout.item_nguoidung, parent, false);
        return new ViewHolder(contactView);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {
        final NguoiDung nguoiDung = listNguoiDung.get(position);
        holder.tvTenND.setText(nguoiDung.getHoVaTen());
        holder.tvSDT.setText(nguoiDung.getSoDienThoai());
        holder.tvUsername.setText(nguoiDung.getUserName());
        holder.itemView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                setPosition(holder.getAdapterPosition());
                return false;
            }
        });

    }

    @Override
    public void onViewRecycled(@NonNull ViewHolder holder) {
        holder.itemView.setOnLongClickListener(null);
        super.onViewRecycled(holder);
    }

    @Override
    public int getItemCount() {
        return listNguoiDung.size();
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public class ViewHolder extends RecyclerView.ViewHolder
            implements View.OnCreateContextMenuListener {
        TextView tvTenND, tvSDT,tvUsername;


        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            tvTenND = itemView.findViewById(R.id.tv_ten_nd);
            tvSDT = itemView.findViewById(R.id.tv_sdt);
            tvUsername = itemView.findViewById(R.id.tv_username);
            itemView.setOnCreateContextMenuListener(this);
        }


        @Override
        public void onCreateContextMenu(ContextMenu contextMenu, View view,
                                        ContextMenu.ContextMenuInfo contextMenuInfo) {
            contextMenu.add(Menu.NONE, R.id.edit,
                    Menu.NONE, R.string.edit);
            contextMenu.add(Menu.NONE, R.id.delete,
                    Menu.NONE, R.string.delete);

        }
    }

    public void setListNguoiDung(List<NguoiDung> listNguoiDung) {
        if (listNguoiDung != null) {
            this.listNguoiDung = new ArrayList<>(listNguoiDung);
            notifyDataSetChanged();
        }
    }
}
