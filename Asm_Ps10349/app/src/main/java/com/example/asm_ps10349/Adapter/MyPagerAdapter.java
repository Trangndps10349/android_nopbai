package com.example.asm_ps10349.Adapter;


import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.example.asm_ps10349.Fragment.DanhSachSVFragment;
import com.example.asm_ps10349.Fragment.MonHocFragment;
import com.example.asm_ps10349.Fragment.XemLichThiFragment;


public class MyPagerAdapter extends FragmentPagerAdapter {
    private static int NUM_ITEMS = 3;

    public MyPagerAdapter(FragmentManager fragmentManager) {
        super(fragmentManager);
    }

    // Returns total number of pages
    @Override
    public int getCount() {
        return NUM_ITEMS;
    }

    // Returns the fragment to display for that page
    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return DanhSachSVFragment.newInstance();
            case 1:
                return MonHocFragment.newInstance();
            case 2:
                return XemLichThiFragment.newInstance();
            default:
                return null;
        }
    }

    @Override
    public CharSequence getPageTitle(int position) {
        switch (position) {
            case 0:
                return "DS SV";
            case 1:
                return "Mon Hoc";
            case 2:
                return "Lich Thi";
            default:
                return null;
        }
    }
}

