package com.example.ps10349_lap3;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

public class AdapterLvCallLog extends BaseAdapter {
    Context context;
    ArrayList<String> callLog;

    public AdapterLvCallLog(Context context, ArrayList<String> callLog) {
        this.context = context;
        this.callLog = callLog;
    }

    @Override
    public int getCount() {
        return callLog.size();
    }

    @Override
    public Object getItem(int i) {
        return callLog.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        if (convertView == null){
            // ket noi custom temview
            LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = layoutInflater.inflate(R.layout.item_lv,null);
            viewHolder = new ViewHolder();
            viewHolder.tvName= convertView.findViewById(R.id.tv_name);
            convertView.setTag(viewHolder);
        }else{
            viewHolder = (ViewHolder) convertView.getTag();
        }
        String Name = callLog.get(i);
        viewHolder.tvName.setText(Name);
        return convertView;
    }
    class ViewHolder{
        TextView tvName;
        public  ViewHolder(){}
    }
}
