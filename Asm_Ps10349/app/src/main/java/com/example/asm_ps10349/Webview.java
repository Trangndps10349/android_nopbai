package com.example.asm_ps10349;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.webkit.WebViewClient;

public class Webview extends AppCompatActivity {
    public static final String LINK_ARGS = "WebView-Link-Args";
    @SuppressLint("SetJavaScriptEnabled")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_webview);
        android.webkit.WebView webview = findViewById(R.id.webview);
        webview.getSettings().setJavaScriptEnabled(true);
        webview.setWebViewClient(new WebViewClient(){
            @Override
            public boolean shouldOverrideUrlLoading(android.webkit.WebView view, String url) {
                return true;
            }
        });

        String url = getIntent().getStringExtra(LINK_ARGS);
        webview.loadUrl(url);
    }
    }

