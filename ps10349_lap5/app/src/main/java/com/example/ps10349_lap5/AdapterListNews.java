package com.example.ps10349_lap5;

import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

public class AdapterListNews extends RecyclerView.Adapter<AdapterListNews.ViewHolder> {

    public interface Listener {
        void onClickNews(News news);
    }

    private List<News> listNews = new ArrayList<>();
    private Listener listener;

    public AdapterListNews(Listener listener, List<News> listNews) {
        this.listener = listener;
        this.listNews = listNews;
    }

    public AdapterListNews(Listener listener) {
        this.listener = listener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View contactView = layoutInflater.inflate(R.layout.item_news, parent, false);
        return new ViewHolder(contactView);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        final News news = listNews.get(position);
        holder.tvTitle.setText(news.getTitle());
        holder.tvPubDate.setText(news.getPubDate());
        holder.tvDescription.setText(news.getDescription());
        holder.tvLink.setText(news.getLink());
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (listener != null) {
                    listener.onClickNews(news);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return listNews.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        TextView tvTitle, tvPubDate, tvDescription, tvLink;

        ViewHolder(@NonNull View itemView) {
            super(itemView);
            tvTitle = itemView.findViewById(R.id.tv_title);
            tvPubDate = itemView.findViewById(R.id.tv_pubdate);
            tvDescription = itemView.findViewById(R.id.tv_description);
            tvLink = itemView.findViewById(R.id.tv_link);
        }
    }

    public void setListNews(List<News> listNews) {
        if (listNews != null) {
            this.listNews = new ArrayList<>(listNews);
            notifyDataSetChanged();
        }
    }
}
