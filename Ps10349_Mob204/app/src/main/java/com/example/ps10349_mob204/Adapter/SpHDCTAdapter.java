package com.example.ps10349_mob204.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.ps10349_mob204.Model.HoaDon;
import com.example.ps10349_mob204.Model.TheLoai;
import com.example.ps10349_mob204.R;

import java.util.List;

public class SpHDCTAdapter extends BaseAdapter {
    Context context;
    List<HoaDon>list;

    public SpHDCTAdapter(Context context, List<HoaDon> list) {
        this.context = context;
        this.list = list;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int i) {
        return list.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        view = inflater.inflate(R.layout.item_spiner_hdct,null);
        TextView tvId = view.findViewById(R.id.tv_so_thu_tu_hdct);
        TextView tvMaHD = view.findViewById(R.id.tv_sp_ma_hoa_don);
        HoaDon hoaDon = list.get(i);
        int id = i+1;
        tvId.setText(id+"");
        tvMaHD.setText(hoaDon.getMaHoaDon());
        return view;
    }

    public void setList(List<HoaDon>list){
        this.list= list;
        notifyDataSetChanged();
    }

}
