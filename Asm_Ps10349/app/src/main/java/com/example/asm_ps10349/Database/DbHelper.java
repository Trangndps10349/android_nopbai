package com.example.asm_ps10349.Database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import androidx.annotation.Nullable;

public class DbHelper extends SQLiteOpenHelper {

    public DbHelper(@Nullable Context context) {
        super(context, "dangkymonhoc", null,2);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String sqlmonhoc = "create table monhoc (" +
                "idmonhoc text primary key," +
                "tenmonhoc text," +
                "lichhoc text ) ";
        db.execSQL(sqlmonhoc);

        String sqllichthi = "create table lichthi (" +
                "idlichthi text primary key ," +
                "tenlichthi text," +
                "ngaythi text," +
                "mamon text)";
                db.execSQL(sqllichthi);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
    sqLiteDatabase.execSQL("drop table if exists monhoc");
    sqLiteDatabase.execSQL("drop table if exists lichthi");
    onCreate(sqLiteDatabase);
    }
}
