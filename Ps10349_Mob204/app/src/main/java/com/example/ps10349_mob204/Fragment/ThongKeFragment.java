package com.example.ps10349_mob204.Fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.ps10349_mob204.R;
import com.google.android.material.bottomnavigation.BottomNavigationView;

public class ThongKeFragment extends Fragment {
    BottomNavigationView bottomNavigationView;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_thong_ke, container, false);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        bottomNavigationView = view.findViewById(R.id.bottom_navigation);
        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                Fragment fragment = null;
                switch (menuItem.getItemId()) {
                    case R.id.item_top_sp:
                        // do something here
                        fragment = new ChildFragmentThongKeTopSp();
                        break;
                    case R.id.item_thongke:
                        // do something here
                        fragment = new ChildFragmentThongKe();
                        break;
                }
                getFragmentManager().beginTransaction().replace(R.id.framethongke, fragment).commit();
                return true;


            }
        });
        bottomNavigationView.setSelectedItemId(R.id.item_top_sp);
    }

}
