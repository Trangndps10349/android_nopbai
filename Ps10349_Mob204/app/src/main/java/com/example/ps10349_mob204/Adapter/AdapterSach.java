package com.example.ps10349_mob204.Adapter;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.ps10349_mob204.Model.Sach;
import com.example.ps10349_mob204.R;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class AdapterSach extends RecyclerView.Adapter<AdapterSach.ViewHolder> implements Filterable {
    private List<Sach> listSach;
    private List<Sach> listSachSort;
    private int position = -1;

    public AdapterSach(List<Sach> listSach) {
        this.listSach = listSach;
        listSachSort = new ArrayList<>(listSach);
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View contactView = layoutInflater.inflate(R.layout.item_sach, parent, false);
        return new ViewHolder(contactView);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int position) {
        final Sach sach = listSach.get(position);
        holder.tvMaSach.setText(sach.getMaSach());
        holder.itemView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                setPosition(holder.getAdapterPosition());
                return false;
            }
        });
    }

    @Override
    public void onViewRecycled(@NonNull ViewHolder holder) {
        holder.itemView.setOnLongClickListener(null);
        super.onViewRecycled(holder);
    }

    @Override
    public int getItemCount() {
        return listSach.size();
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }
//////////// tim sach
    @Override
    public Filter getFilter() {
        return sachFilter;
    }
    private Filter sachFilter = new Filter() {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            List<Sach> filterSach = new ArrayList<>();
            if (constraint == null || constraint.length() == 0){
                filterSach.addAll(listSachSort);
            }else {
                String filterPattenr = constraint.toString().toLowerCase().trim();
                for (Sach sach : listSachSort){
                    if (sach.getMaSach().toLowerCase().contains(filterPattenr)){
                        filterSach.add(sach);
                    }
                }
            }
            FilterResults results = new FilterResults();
            results.values = filterSach;
            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            listSach.clear();
            listSach.addAll((List) results.values);
            notifyDataSetChanged();
        }
    };

    public class ViewHolder extends RecyclerView.ViewHolder
            implements View.OnCreateContextMenuListener {
        TextView tvMaSach;

        public ViewHolder(View view) {
            super(view);
            tvMaSach = view.findViewById(R.id.tv_ma_sach);
            itemView.setOnCreateContextMenuListener(this);

        }

        @Override
        public void onCreateContextMenu(ContextMenu contextMenu, View view, ContextMenu.ContextMenuInfo contextMenuInfo) {
            contextMenu.add(Menu.NONE, R.id.edit_sach,
                    Menu.NONE, R.string.edit);
            contextMenu.add(Menu.NONE, R.id.delete_sach,
                    Menu.NONE, R.string.delete);

        }
    }

    public void setListSach(List<Sach> listSach) {
        if (listSach != null) {
            this.listSach = new ArrayList<>(listSach);
            notifyDataSetChanged();
        }
    }

}
