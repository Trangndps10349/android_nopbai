package com.example.ps10349_mob204;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.ps10349_mob204.Dao.NguoiDungDao;
import com.example.ps10349_mob204.Model.NguoiDung;

public class ChangePassWordActivity extends AppCompatActivity {
    EditText edtPass, edtRePass;
    Button btnChange, btnCanCel;
    NguoiDungDao nguoiDungDao;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_pass_word);
        anhxa();
        btnChange.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                changePassword();
            }
        });
        btnCanCel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                edtPass.setText("");
                edtRePass.setText("");
            }
        });


    }

    public void anhxa() {
        edtPass = findViewById(R.id.edtPassword);
        edtRePass = findViewById(R.id.edtPasswordre);
        btnChange = findViewById(R.id.btnChange);
        btnCanCel = findViewById(R.id.btnCancel);
    }

    public int validateForm() {
        int check = 1;
        if (edtPass.getText().length() == 0 || edtRePass.getText().length() == 0) {
            Toast.makeText(this, "Ban phai nhap day du thong tin", Toast.LENGTH_SHORT).show();
            check = -1;
        } else {
            String pass = edtPass.getText().toString();
            String repass = edtRePass.getText().toString();
            if (!pass.equalsIgnoreCase(repass)) {
                Toast.makeText(this, "Mat khau khong trung khop", Toast.LENGTH_SHORT).show();
                check = -1;
            }
        }
        return check;
    }

    public void changePassword() {
        SharedPreferences preferences = getSharedPreferences("USER_FILE", Context.MODE_PRIVATE);
        String strUserName = preferences.getString("USERNAME", "");
        nguoiDungDao = new NguoiDungDao(this);
        NguoiDung user = new NguoiDung(strUserName, edtPass.getText().toString(), "", "");
        try {
            if (validateForm() > 0) {
                if (nguoiDungDao.changePassWord(user) > 0) {
                    Toast.makeText(this, "Luu thanh cong ", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(this, "Luu that bai", Toast.LENGTH_SHORT).show();
                }
            }
            finish();
        } catch (Exception ex) {
            Log.e("Error", ex.toString());
        }

    }
}
