package com.example.ps10349_mob204.Fragment;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.ps10349_mob204.Adapter.AdapterTheLoai;
import com.example.ps10349_mob204.Dao.TheLoaiDao;
import com.example.ps10349_mob204.Model.TheLoai;
import com.example.ps10349_mob204.R;

import java.util.ArrayList;

public class ChildFragmentTheLoaiSach extends Fragment {
    AdapterTheLoai adapterTheLoai;
    RecyclerView recyclerViewTheLoai;
    TheLoaiDao theLoaiDao;
    ArrayList<TheLoai> danhSachTheLoai;

    public static ChildFragmentTheLoaiSach newInstance() {
        Bundle args = new Bundle();
        ChildFragmentTheLoaiSach fragment = new ChildFragmentTheLoaiSach();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.child_fragment_sach_theloai, container, false);
        danhSachTheLoai = new ArrayList<>();
        theLoaiDao = new TheLoaiDao(getContext());
        recyclerViewTheLoai = v.findViewById(R.id.recyclerview_chitiet_theloai);
        recyclerViewTheLoai.setLayoutManager(new LinearLayoutManager(getContext()));
        adapterTheLoai = new AdapterTheLoai(danhSachTheLoai);
        recyclerViewTheLoai.setAdapter(adapterTheLoai);
        capNhatGiaoDienTheLoai();
        registerForContextMenu(recyclerViewTheLoai);

        return v;
    }
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        view.findViewById(R.id.fab_them_tl).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ThemTheLoaiDiaLog();
            }
        });

    }

    @Override
    public boolean onContextItemSelected( MenuItem item) {

        switch (item.getItemId()) {
            case R.id.edit_the_loai:
                if (danhSachTheLoai != null
                        && danhSachTheLoai.size() > adapterTheLoai.getPosition()
                        && adapterTheLoai.getPosition() >= 0) {
                    SuaTheLoaiDialog(danhSachTheLoai.get(adapterTheLoai.getPosition()));
                }
                return true;
            case R.id.delete_the_loai:
                if (danhSachTheLoai != null
                        && danhSachTheLoai.size() > adapterTheLoai.getPosition()
                        && adapterTheLoai.getPosition() >= 0) {
                    XoaTheLoaiDialog();
                }
                return true;
        }
        return super.onContextItemSelected(item);
    }

    private void ThemTheLoaiDiaLog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getLayoutInflater().getContext());
        final LayoutInflater inflater = getActivity().getLayoutInflater();
        View v = inflater.inflate(R.layout.dialog_them_the_loai, null);

        final EditText edtTenTL = v.findViewById(R.id.edt_them_ten_tl);
        final EditText edtMaTL = v.findViewById(R.id.edt_them_ma_tl);
        final EditText edtViTri = v.findViewById(R.id.edt_them_vi_tri);
        final EditText edtMota = v.findViewById(R.id.edt_them_mota);

        builder.setTitle("THEM THE LOAI");
        builder.setView(v);

        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                TheLoai theLoai = new TheLoai();
                String txtTenTL = edtTenTL.getText().toString();
                String txtMaTL = edtMaTL.getText().toString();
                String txtViTri = edtViTri.getText().toString();
                String txtMoTa = edtMota.getText().toString();

                if (TextUtils.isEmpty(txtTenTL)) {
                    Toast.makeText(getContext(), "Ten the loai trong", Toast.LENGTH_SHORT).show();
                }
                if (TextUtils.isEmpty(txtMaTL)) {
                    Toast.makeText(getContext(), "Ma the loai trong", Toast.LENGTH_SHORT).show();
                }
                if (TextUtils.isEmpty(txtViTri)) {
                    Toast.makeText(getContext(), "Vi tri the loai trong", Toast.LENGTH_SHORT).show();
                } else {
                    theLoai.setTenTheLoai(txtTenTL);
                    theLoai.setMaTheLoai(txtMaTL);
                    theLoai.setViTri(Integer.parseInt(txtViTri));
                    theLoai.setMoTa(txtMoTa);

                    if (theLoaiDao.inserTheLoai(theLoai) == -1) {
                        Toast.makeText(getContext(), "Them The Loai ko thanh cong", Toast.LENGTH_SHORT).show();
                    } else {
                        danhSachTheLoai.add(theLoai);
                        capNhatGiaoDienTheLoai();
                        Toast.makeText(getContext(), "Them thanh cong", Toast.LENGTH_SHORT).show();
                    }
                }

            }
        });

        builder.setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    private void SuaTheLoaiDialog(TheLoai theLoai) {

        AlertDialog.Builder builder = new AlertDialog.Builder(getLayoutInflater().getContext());
        final LayoutInflater inflater = getActivity().getLayoutInflater();
        View v = inflater.inflate(R.layout.dialog_sua_the_loai, null);

        final EditText edtTenTL = v.findViewById(R.id.edt_sua_ten_tl);
        final EditText edtMaTL = v.findViewById(R.id.edt_sua_ma_tl);
        final EditText edtViTri = v.findViewById(R.id.edt_sua_vi_tri);
        final EditText edtMota = v.findViewById(R.id.edt_sua_mota);

        if (theLoai != null) {
            edtTenTL.setText(theLoai.getTenTheLoai());
            edtMaTL.setText(theLoai.getMaTheLoai());
            edtViTri.setText(String.valueOf(theLoai.getViTri()));
            edtMota.setText(theLoai.getMoTa());

        } else {
            edtTenTL.setText("");
            edtMaTL.setText("");
            edtViTri.setText("");
            edtMota.setText("");

        }

        builder.setTitle("SUA THE LOAI");
        builder.setView(v);

        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                TheLoai theLoai = new TheLoai();

                String txtTenTL = edtTenTL.getText().toString();
                String txtMaTL = edtMaTL.getText().toString();
                String txtViTri = edtViTri.getText().toString();
                String txtMoTa = edtMota.getText().toString();


                //check validate
                if (TextUtils.isEmpty(txtTenTL)) {
                    Toast.makeText(getContext(), "Ten the loai trong", Toast.LENGTH_SHORT).show();
                }
                if (TextUtils.isEmpty(txtMaTL)) {
                    Toast.makeText(getContext(), "Ma the loai trong", Toast.LENGTH_SHORT).show();
                }
                if (TextUtils.isEmpty(txtViTri)) {
                    Toast.makeText(getContext(), "Vi tri the loai trong", Toast.LENGTH_SHORT).show();
                } else {
                    //insert
                    theLoai.setTenTheLoai(txtTenTL);
                    theLoai.setMaTheLoai(txtMaTL);
                    theLoai.setViTri(Integer.parseInt(txtViTri));
                    theLoai.setMoTa(txtMoTa);
                    if (theLoaiDao.updateTheloai(theLoai) == -1) {
                        Toast.makeText(getContext(), "Sua the loai ko thanh cong", Toast.LENGTH_SHORT).show();
                    } else {
                        danhSachTheLoai.add(theLoai);
                        capNhatGiaoDienTheLoai();
                        Toast.makeText(getContext(), "Sua thanh cong", Toast.LENGTH_SHORT).show();
                    }

                }
            }

        });
        builder.setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    private void XoaTheLoaiDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getLayoutInflater().getContext());
        builder.setTitle("Xoa");
        builder.setMessage("Xoa The Loai?");
        builder.setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });
        builder.setPositiveButton("Xoa", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                theLoaiDao.deleteTheLoai(danhSachTheLoai.get(adapterTheLoai.getPosition()));
                capNhatGiaoDienTheLoai();
            }
        });
        builder.show();
    }
    private void capNhatGiaoDienTheLoai() {
        danhSachTheLoai.clear();
        danhSachTheLoai = (ArrayList<TheLoai>) theLoaiDao.getAllTheLoai();
        adapterTheLoai.setListTheLoai(danhSachTheLoai);
    }
}
