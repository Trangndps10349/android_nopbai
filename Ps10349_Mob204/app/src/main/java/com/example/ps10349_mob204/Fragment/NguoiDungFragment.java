package com.example.ps10349_mob204.Fragment;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.SearchView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.ps10349_mob204.Adapter.AdapterNguoiDung;
import com.example.ps10349_mob204.Dao.NguoiDungDao;
import com.example.ps10349_mob204.Model.NguoiDung;
import com.example.ps10349_mob204.R;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

public class NguoiDungFragment extends Fragment {

    AdapterNguoiDung adapterNguoiDung;
    RecyclerView nguoiDungRecycleview;
    NguoiDungDao nguoiDungDao;
    ArrayList<NguoiDung> danhSachNguoiDung;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_nguoi_dung, container, false);
        danhSachNguoiDung = new ArrayList<>();
        nguoiDungDao = new NguoiDungDao(getContext());
        nguoiDungRecycleview = view.findViewById(R.id.recyclerview_nguoi_dung);
        nguoiDungRecycleview.setLayoutManager(new LinearLayoutManager(getContext()));
        adapterNguoiDung = new AdapterNguoiDung(danhSachNguoiDung);
        nguoiDungRecycleview.setAdapter(adapterNguoiDung);
        capNhatGiaoDienNguoiDung();
        registerForContextMenu(nguoiDungRecycleview);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        view.findViewById(R.id.fab_them_nd).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ThemNguoiDungDialog();
            }
        });
    }

    @Override
    public boolean onContextItemSelected( MenuItem item) {

        switch (item.getItemId()) {
            case R.id.edit:
                if (danhSachNguoiDung != null
                        && danhSachNguoiDung.size() > adapterNguoiDung.getPosition()
                        && adapterNguoiDung.getPosition() >= 0) {
                    SuaNguoiDungDialog(danhSachNguoiDung.get(adapterNguoiDung.getPosition()));
                }
                return true;

            case R.id.delete:
                if (danhSachNguoiDung != null
                        && danhSachNguoiDung.size() > adapterNguoiDung.getPosition()
                        && adapterNguoiDung.getPosition() >= 0) {
                    XoaNguoiDungDialog();
                }
                return true;
        }
        return super.onContextItemSelected(item);
    }


    public void ThemNguoiDungDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getLayoutInflater().getContext());
        final LayoutInflater inflater = getActivity().getLayoutInflater();
        View v = inflater.inflate(R.layout.dialog_them_nd, null);
        final EditText edtTenNd = v.findViewById(R.id.edt_them_ten_nd);
        final EditText edtEmail = v.findViewById(R.id.edt_them_email);
        final EditText edtNgaySinh = v.findViewById(R.id.edt_them_ngay_sinh);
        final EditText edtSDT = v.findViewById(R.id.edt_them_sdt);
        final EditText edtUsername = v.findViewById(R.id.edt_them_username);
        final EditText edtPassword = v.findViewById(R.id.edt_them_password);

        builder.setTitle("THEM NGUOI DUNG");
        builder.setView(v);
        edtNgaySinh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Calendar calendar = Calendar.getInstance();
                int day = calendar.get(Calendar.DAY_OF_MONTH);
                int month = calendar.get(Calendar.MONTH);
                int year = calendar.get(Calendar.YEAR);
                DatePickerDialog datePickerDialog = new DatePickerDialog(getContext(), new DatePickerDialog.OnDateSetListener() {
                    @SuppressLint("SetTextI18n")
                    @Override
                    public void onDateSet(DatePicker datePicker, int year, int month, int day) {
                        //i: nam, i1: thang, i2 Ngay
                        calendar.set(year, month, day);
                        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");
                        edtNgaySinh.setText(simpleDateFormat.format(calendar.getTime()));
                    }
                }, year, month,day );
                datePickerDialog.show();
            }

        });

        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                NguoiDung nguoiDung = new NguoiDung();
                String txtTenNd = edtTenNd.getText().toString();
                String txtEmail = edtEmail.getText().toString();
                String txtNgaySinh = edtNgaySinh.getText().toString();
                String txtSDT = edtSDT.getText().toString();
                String txtUsername = edtUsername.getText().toString();
                String txtPassword = edtPassword.getText().toString();

                //check validate
                if (TextUtils.isEmpty(txtTenNd)) {
                    Toast.makeText(getContext(), "Ten Nguoi dung trong", Toast.LENGTH_SHORT).show();
                }
                if (TextUtils.isEmpty(txtEmail)) {
                    Toast.makeText(getContext(), "Email Nguoi dung trong", Toast.LENGTH_SHORT).show();
                }
                if (TextUtils.isEmpty(txtNgaySinh)) {
                    Toast.makeText(getContext(), "Ngay Sinh Nguoi dung trong", Toast.LENGTH_SHORT).show();
                }
                if (TextUtils.isEmpty(txtSDT)) {
                    Toast.makeText(getContext(), "SDT Nguoi dung trong", Toast.LENGTH_SHORT).show();
                }
                if (TextUtils.isEmpty(txtUsername)) {
                    Toast.makeText(getContext(), "Username Nguoi dung trong", Toast.LENGTH_SHORT).show();
                }
                if (TextUtils.isEmpty(txtPassword)) {
                    Toast.makeText(getContext(), "Passwork Nguoi dung trong", Toast.LENGTH_SHORT).show();
                } else {
                    //insert
                    nguoiDung.setHoVaTen(txtTenNd);
                    nguoiDung.setEmail(txtEmail);
                    nguoiDung.setNgaySinh(txtNgaySinh);
                    nguoiDung.setUserName(txtUsername);
                    nguoiDung.setSoDienThoai(txtSDT);
                    nguoiDung.setPassWord(txtPassword);

                    if (nguoiDungDao.inserNguoiDung(nguoiDung) == -1) {
                        Toast.makeText(getContext(), "Them Nguoi Dung ko thanh cong", Toast.LENGTH_SHORT).show();
                    } else {
                        danhSachNguoiDung.add(nguoiDung);
                        capNhatGiaoDienNguoiDung();
                        Toast.makeText(getContext(), "Them thanh cong", Toast.LENGTH_SHORT).show();
                    }

                }
            }
        });
        builder.setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });
        AlertDialog dialog = builder.create();
        dialog.show();

    }


    private void capNhatGiaoDienNguoiDung() {
        danhSachNguoiDung.clear();
        danhSachNguoiDung = (ArrayList<NguoiDung>) nguoiDungDao.getAllNguoiDung();
        adapterNguoiDung.setListNguoiDung(danhSachNguoiDung);
    }

    private void SuaNguoiDungDialog(NguoiDung nguoiDung) {

        AlertDialog.Builder builder = new AlertDialog.Builder(getLayoutInflater().getContext());
        final LayoutInflater inflater = getActivity().getLayoutInflater();
        View v = inflater.inflate(R.layout.dialog_update_nd, null);

        final EditText edtTenNd = v.findViewById(R.id.edt_update_ten_nd);
        final EditText edtEmail = v.findViewById(R.id.edt_update_email);
        final EditText edtNgaySinh = v.findViewById(R.id.edt_update_ngay_sinh);
        final EditText edtSDT = v.findViewById(R.id.edt_update_sdt);
        final EditText edtUsername = v.findViewById(R.id.edt_update_username);
        final EditText edtPassword = v.findViewById(R.id.edt_update_password);



        if (nguoiDung != null) {
            edtTenNd.setText(nguoiDung.getHoVaTen());
            edtEmail.setText(nguoiDung.getEmail());
            edtNgaySinh.setText(nguoiDung.getNgaySinh());
            edtSDT.setText(nguoiDung.getSoDienThoai());
            edtUsername.setText(nguoiDung.getUserName());
            edtPassword.setText(nguoiDung.getPassWord());
        } else {
            edtEmail.setText("");
            edtTenNd.setText("");
            edtEmail.setText("");
            edtNgaySinh.setText("");
            edtSDT.setText("");
            edtUsername.setText("");
            edtPassword.setText("");
        }
        edtNgaySinh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Calendar calendar = Calendar.getInstance();
                int day = calendar.get(Calendar.DAY_OF_MONTH);
                int month = calendar.get(Calendar.MONTH);
                int year = calendar.get(Calendar.YEAR);
                DatePickerDialog datePickerDialog = new DatePickerDialog(getContext(), new DatePickerDialog.OnDateSetListener() {
                    @SuppressLint("SetTextI18n")
                    @Override
                    public void onDateSet(DatePicker datePicker, int year, int month, int day) {
                        //i: nam, i1: thang, i2 Ngay
                        calendar.set(year, month, day);
                        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");
                        edtNgaySinh.setText(simpleDateFormat.format(calendar.getTime()));
                    }
                }, year, month,day );
                datePickerDialog.show();
            }

        });

        builder.setTitle("SUA NGUOI DUNG");
        builder.setView(v);

        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                NguoiDung nguoiDung = new NguoiDung();

                String txtTenNd = edtTenNd.getText().toString();
                String txtEmail = edtEmail.getText().toString();
                String txtNgaySinh = edtNgaySinh.getText().toString();
                String txtSDT = edtSDT.getText().toString();
                String txtUsername = edtUsername.getText().toString();
                String txtPassword = edtPassword.getText().toString();

                //check validate
                if (TextUtils.isEmpty(txtTenNd)) {
                    Toast.makeText(getContext(), "Ten Nguoi dung trong", Toast.LENGTH_SHORT).show();
                }
                if (TextUtils.isEmpty(txtEmail)) {
                    Toast.makeText(getContext(), "Email Nguoi dung trong", Toast.LENGTH_SHORT).show();
                }
                if (TextUtils.isEmpty(txtNgaySinh)) {
                    Toast.makeText(getContext(), "Ngay Sinh Nguoi dung trong", Toast.LENGTH_SHORT).show();
                }
                if (TextUtils.isEmpty(txtSDT)) {
                    Toast.makeText(getContext(), "SDT Nguoi dung trong", Toast.LENGTH_SHORT).show();
                }
                if (TextUtils.isEmpty(txtUsername)) {
                    Toast.makeText(getContext(), "Username Nguoi dung trong", Toast.LENGTH_SHORT).show();
                }
                if (TextUtils.isEmpty(txtPassword)) {
                    Toast.makeText(getContext(), "Passwork Nguoi dung trong", Toast.LENGTH_SHORT).show();
                } else {
                    //insert
                    nguoiDung.setHoVaTen(txtTenNd);
                    nguoiDung.setEmail(txtEmail);
                    nguoiDung.setNgaySinh(txtNgaySinh);
                    nguoiDung.setUserName(txtUsername);
                    nguoiDung.setSoDienThoai(txtSDT);
                    nguoiDung.setPassWord(txtPassword);

                    if (nguoiDungDao.updateNguoiDung(nguoiDung) == -1) {
                        Toast.makeText(getContext(), "Sua Nguoi Dung ko thanh cong", Toast.LENGTH_SHORT).show();
                    } else {
                        danhSachNguoiDung.add(nguoiDung);
                        capNhatGiaoDienNguoiDung();
                        Toast.makeText(getContext(), "Sua thanh cong", Toast.LENGTH_SHORT).show();
                    }

                }
            }

        });
        builder.setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });
        AlertDialog dialog = builder.create();
        dialog.show();


    }

    private void XoaNguoiDungDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getLayoutInflater().getContext());
        builder.setTitle("Xoa");
        builder.setMessage("Xoa Nguoi Dung?");
        builder.setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });
        builder.setPositiveButton("Xoa", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                nguoiDungDao.deleteNguoiDung(danhSachNguoiDung.get(adapterNguoiDung.getPosition()));
                capNhatGiaoDienNguoiDung();
            }
        });
        builder.show();
    }
// search nguoi dung

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);

    }

}
