package com.example.ps10349_lap5;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.ListAdapter;
import androidx.recyclerview.widget.RecyclerView;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.widget.ArrayAdapter;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import static com.example.ps10349_lap5.WebView.LINK_ARGS;

public class MainActivity extends AppCompatActivity implements AdapterListNews.Listener {
    private RecyclerView recyclerView;
    private AdapterListNews adapterListNews;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        recyclerView = findViewById(R.id.list_news);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        adapterListNews = new AdapterListNews(this);
        recyclerView.setAdapter(adapterListNews);

        NewsSeedAsyncTask newsSeedAsyncTask = new NewsSeedAsyncTask();
        newsSeedAsyncTask.execute();
    }

    @Override
    public void onClickNews(News news) {
        Intent intent = new Intent(this, WebView.class);
        intent.putExtra(LINK_ARGS, news.getLink());
        startActivity(intent);
    }

    @SuppressLint("StaticFieldLeak")
    class NewsSeedAsyncTask extends AsyncTask<Void, Void, List<News>> {

        @Override
        protected List<News> doInBackground(Void... voids) {
            try {
                URL url = new URL("https://vnexpress.net/rss/thoi-su.rss");
                HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
                InputStream inputStream = httpURLConnection.getInputStream();
                return NewsReader.listNews(inputStream);
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(List<News> news) {
            super.onPostExecute(news);
            if (news != null) {
                adapterListNews.setListNews(news);
            }
        }
    }
}
