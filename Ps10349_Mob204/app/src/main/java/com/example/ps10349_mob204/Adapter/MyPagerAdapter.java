package com.example.ps10349_mob204.Adapter;


import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.example.ps10349_mob204.Fragment.ChildFragmentChiTietSach;
import com.example.ps10349_mob204.Fragment.ChildFragmentTheLoaiSach;

public class MyPagerAdapter extends FragmentPagerAdapter {
    private static int NUM_ITEMS = 2;



    public MyPagerAdapter(@NonNull FragmentManager fm, int behavior) {
        super(fm, behavior);
    }


    // Returns total number of pages
    @Override
    public int getCount() {
        return NUM_ITEMS;
    }

    // Returns the fragment to display for that page
    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return ChildFragmentChiTietSach.newInstance();
            case 1:
                return ChildFragmentTheLoaiSach.newInstance();
            default:
                return null;
        }
    }

    @Override
    public CharSequence getPageTitle(int position) {
        switch (position) {
            case 0:
                return "Sach";
            case 1:
                return "The Loai";
            default:
                return null;
        }
    }
}

