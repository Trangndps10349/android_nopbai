package com.example.asm_ps10349.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.asm_ps10349.Model.LichThi;
import com.example.asm_ps10349.Model.MonHoc;
import com.example.asm_ps10349.R;

import java.util.ArrayList;

public class AdapterLichThi extends BaseAdapter {
    public interface LichThiListener{
        void xoaLichThi(String idLichThi);
        void suaLichThi(LichThi lichThi);
    }
    Context context;
    ArrayList<LichThi>dsLichThi;
    LichThiListener lichThiListener;

    public AdapterLichThi(Context context,  ArrayList<LichThi>dsLichThi) {
        this.context = context;
        this.dsLichThi = dsLichThi;
    }


    @Override
    public int getCount() {
        return dsLichThi.size();
    }

    @Override
    public Object getItem(int position) {
        return dsLichThi.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        if (convertView == null){
            // ket noi custom temview
            LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = layoutInflater.inflate(R.layout.item_lich_thi,null);
            viewHolder = new ViewHolder();
            viewHolder.tvMaLichThi=  convertView.findViewById(R.id.tv_item_id_lich_thi);
            viewHolder.tvTenLichThi= convertView.findViewById(R.id.tv_item_ten_lich_thi);
            viewHolder.tvNgayLichThi= convertView.findViewById(R.id.tv_item_ngay_thi);
            viewHolder.imgSua= convertView.findViewById(R.id.img_edit_lich_thi);
            viewHolder.imgXoa= convertView.findViewById(R.id.img_delete_lich_thi);
            convertView.setTag(viewHolder);
        }else{
            viewHolder = (ViewHolder) convertView.getTag();
        }
        final LichThi lichThi = dsLichThi.get(position);

        viewHolder.tvMaLichThi.setText(lichThi.getMaLichThi());
        viewHolder.tvTenLichThi.setText(lichThi.getTenMonThi());
        viewHolder.tvNgayLichThi.setText(lichThi.getNgayThi());
        viewHolder.tvMaMonThi.setText(lichThi.getMaMonThi());

        viewHolder.imgSua.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                lichThiListener.suaLichThi(lichThi);
            }
        });
        viewHolder.imgXoa.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                lichThiListener.xoaLichThi(dsLichThi.get(position).getMaLichThi());
            }
        });
        return convertView;
    }
    class ViewHolder{
    ImageView imgSua,imgXoa;
    TextView tvMaLichThi,tvTenLichThi,tvNgayLichThi,tvMaMonThi;
    public  ViewHolder(){}
    }
}
