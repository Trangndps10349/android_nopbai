package com.example.nestedrecyclerview.Interface;

import com.example.nestedrecyclerview.Model.ItemGroup;

import java.util.List;

public interface IFirebaseLoadListener {
   void onFirebaseLoadSuccess(List<ItemGroup>itemGroupList);
   void onFirebaseLoadFailed(String message);
}
