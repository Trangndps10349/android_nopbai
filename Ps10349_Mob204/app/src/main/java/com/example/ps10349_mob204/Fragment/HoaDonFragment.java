package com.example.ps10349_mob204.Fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.ps10349_mob204.R;
import com.google.android.material.bottomnavigation.BottomNavigationView;

public class HoaDonFragment extends Fragment {
    BottomNavigationView bottomNavigationView;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_hoa_don, container, false);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        bottomNavigationView = view.findViewById(R.id.bottom_navigation_hoa_don);
        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                Fragment fragment = null;
                switch (menuItem.getItemId()) {
                    case R.id.item_hoa_don:
                        // do something here
                        fragment = new ChildFragmentHoaDon();
                        break;
                    case R.id.item_hdct:
                        // do something here
                        fragment = new ChildFragmentHDCT();
                        break;
                }
                getFragmentManager().beginTransaction().replace(R.id.frame_hoa_don, fragment).commit();
                return true;


            }
        });
        bottomNavigationView.setSelectedItemId(R.id.item_hoa_don);
    }
}

