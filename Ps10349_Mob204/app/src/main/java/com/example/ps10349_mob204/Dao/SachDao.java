package com.example.ps10349_mob204.Dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.example.ps10349_mob204.Database.DbHelper;
import com.example.ps10349_mob204.Model.Sach;

import java.util.ArrayList;
import java.util.List;

public class SachDao {
    private SQLiteDatabase db;
    private DbHelper dbHelper;

    public SachDao(Context context) {
        dbHelper = new DbHelper(context);
        db = dbHelper.getWritableDatabase();
    }

    //inser
    public int insertSach(Sach sach) {
        ContentValues values = new ContentValues();
        values.put("masach", sach.getMaSach());
        values.put("matheloai", sach.getMaTheLoai());
        values.put("tensach", sach.getTieuDeSach());
        values.put("tacgia", sach.getTacGia());
        values.put("nxb", sach.getNhaXuatBan());
        values.put("giabia", sach.getGiaBia());
        values.put("soluong", sach.getSoLuong());
        if (checkPrimaryKey(sach.getMaSach())) {
            int result = db.update("SachTb", values, "masach=?", new String[]{sach.getMaSach()});
            if (result == 0) {
                return 1;
            }
        } else {
            try {
                if (db.insert("SachTb", null, values) == -1) {
                    return -1;
                }
            } catch (Exception ex) {
                Log.e("SachDao", ex.toString());
            }
        }

        return 1;
    }

    //check
    public boolean checkPrimaryKey(String strPrimaryKey) {
        //SELECT
        String[] columns = {"masach"};
        //WHERE clause
        String selection = "masach=?";
        //WHERE clause arguments
        String[] selectionArgs = {strPrimaryKey};
        Cursor c = null;
        try {
            c = db.query("SachTb", columns, selection, selectionArgs, null, null, null);
            c.moveToFirst();
            int i = c.getCount();
            c.close();
            return i > 0;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }

    }

    //get all
    public List<Sach> getAllSach() {
        List<Sach> dsSach = new ArrayList<>();
        Cursor c = db.query("SachTb", null, null, null, null, null, null);
        c.moveToFirst();
        while (c.isAfterLast() == false) {
            Sach sach = new Sach();
            sach.setMaSach(c.getString(0));
            sach.setMaTheLoai(c.getString(1));
            sach.setTieuDeSach(c.getString(2));
            sach.setTacGia(c.getString(3));
            sach.setNhaXuatBan(c.getString(4));
            sach.setGiaBia(c.getDouble(5));
            sach.setSoLuong(c.getInt(6));
            dsSach.add(sach);
            Log.d("====", sach.toString());
            c.moveToNext();
        }
        c.close();
        return dsSach;
    }

    //update
    public int updateSach(Sach sach) {
        ContentValues values = new ContentValues();
        values.put("masach", sach.getMaSach());
        values.put("matheloai", sach.getMaTheLoai());
        values.put("tensach", sach.getTieuDeSach());
        values.put("tacgia", sach.getTacGia());
        values.put("nxb", sach.getNhaXuatBan());
        values.put("giabia", sach.getGiaBia());
        values.put("soluong", sach.getSoLuong());
        int result = db.update("SachTb", values, "masach=?", new String[]{sach.getMaSach()});
        if (result == 0) {
            return -1;
        }
        return 1;
    }

    //delete
    public int deleteSachById(String maSach) {
        int result = db.delete("SachTb", "masach=?", new String[]{maSach});
        if (result == 0) {
            return -1;
        }
        return 1;
    }

    //check
    public Sach checkBook(String strPrimaryKey) {
        Sach sach = new Sach();
        String[] columns = {"masach"};
        String selection = "masach=?";
        String[] selectionArgs = {strPrimaryKey};
        Cursor c = null;
        try {
            c = db.query("SachTb", columns, selection, selectionArgs, null, null, null);
            c.moveToFirst();
            while (c.isAfterLast() == false) {
                sach.setMaSach(c.getString(0));
                sach.setMaTheLoai(c.getString(1));
                sach.setTieuDeSach(c.getString(2));
                sach.setTacGia(c.getString(3));
                sach.setNhaXuatBan(c.getString(4));
                sach.setGiaBia(c.getDouble(5));
                sach.setSoLuong(c.getInt(6));
                break;
            }
            c.close();
            return sach;
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }

    //getAll by id
    public Sach getSachById(String maSach) {
        Sach sach = null;
        String selection = "masach=?";
        String[] selectionArgs = {maSach};
        Cursor c = db.query("SachTb", null, selection, selectionArgs, null, null, null);
        Log.d("getSachById", "===" + c.getCount());
        c.moveToFirst();
        while (c.isAfterLast() == false) {
            sach = new Sach();
            sach.setMaSach(c.getString(0));
            sach.setMaTheLoai(c.getString(1));
            sach.setTieuDeSach(c.getString(2));
            sach.setTacGia(c.getString(3));
            sach.setNhaXuatBan(c.getString(4));
            sach.setGiaBia(c.getDouble(5));
            sach.setSoLuong(c.getInt(6));
            break;
        }
        c.close();
        return sach;
    }

    //get  top 10 sach
    public List<Sach> getSachTop10(String month) {
        List<Sach> dsSach = new ArrayList<>();
        if (Integer.parseInt(month) < 10) {
            month = "0" + month;
        }
        String sSQL = "SELECT masach, SUM(soluong) as soluong FROM HoaDonChiTietTb " +
                "INNER JOIN HoaDonTb on HoaDonTb.mahoadon =HoaDonChITietTb.mahoadon " +
                "WHERE strftime('%m',HoaDonTb.ngaymua)='" + month + "'" +
                "GROUP BY maSach ORDER BY soluong DESC LIMIT 10";
        Cursor cursor = db.rawQuery(sSQL, null);
        cursor.moveToFirst();
        while (cursor.isAfterLast() == false) {
            Log.d("=======", cursor.getString(0));
            Sach sach = new Sach();
            sach.setMaSach(cursor.getString(0));
            sach.setSoLuong(cursor.getInt(1));
            sach.setGiaBia((double) 0);
            sach.setMaTheLoai("");
            sach.setTieuDeSach("");
            sach.setTacGia("");
            sach.setNhaXuatBan("");
            cursor.moveToNext();
        }
        cursor.close();
        return dsSach;
    }

}
