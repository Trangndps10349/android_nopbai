package com.example.asm_ps10349.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.asm_ps10349.Model.MonHoc;
import com.example.asm_ps10349.R;


import java.util.ArrayList;

public class AdapterSp extends BaseAdapter {
    Context c;
    ArrayList<MonHoc> dsMonhoc;

    public AdapterSp(Context c, ArrayList<MonHoc> dsMonhoc) {
        this.c = c;
        this.dsMonhoc = dsMonhoc;
    }

    @Override
    public int getCount() {
        return dsMonhoc.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) c.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        convertView = inflater.inflate(R.layout.item_sp,null);
        TextView txtId = (TextView)convertView.findViewById(R.id.txtId);
        TextView txtTenLop = (TextView)convertView.findViewById(R.id.txt_ten_mon_thi);
        txtTenLop.setText(dsMonhoc.get(position).getTenMonHoc());
        int id = position+1;
        txtId.setText(id+"");
        return convertView;
    }

    public void updateSinhViens(ArrayList<MonHoc> dsMonhoc) {
        this.dsMonhoc = dsMonhoc;
        notifyDataSetChanged();
    }
}


