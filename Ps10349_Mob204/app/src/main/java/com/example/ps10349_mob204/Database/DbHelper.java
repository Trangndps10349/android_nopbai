package com.example.ps10349_mob204.Database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DbHelper extends SQLiteOpenHelper {

    public DbHelper(Context context) {
        super(context, "quanlithuvien", null, 4);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        // tao bang nguoi dung
        String sqlNguoiDung = "CREATE TABLE NguoiDungTb(" +
                "username text primary key," +
                "password text," +
                "phone text," +
                "hoten text," +
                "email text," +
                "ngaysinh text);";
        db.execSQL(sqlNguoiDung);
        // tao bang the loai
        String sqlTheLoai = "CREATE TABLE TheLoaiTb(" +
                "matheloai text primary key," +
                "tentheloai text," +
                "mota text," +
                "vitri int);";
        db.execSQL(sqlTheLoai);
        // tao bang sach
        String sqlSach = "CREATE TABLE SachTb(" +
                "masach text primary key," +
                "matheloai text," +
                "tensach text," +
                "tacgia text," +
                "nxb text," +
                "giabia double," +
                "soluong integer);";
        db.execSQL(sqlSach);
        //tao bang hoa don
        String sqlHoaDon = "CREATE TABLE HoaDonTb(" +
                "mahoadon text primary key," +
                "ngaymua date) ";
        db.execSQL(sqlHoaDon);
        //tao bang hoa don chi tiec
        String sqlHoaDonChiTiet = "CREATE TABLE HoaDonChiTietTb(" +
                "mahdct integer primary key autoincrement," +
                "mahoadon text not null," +
                "masach text not null," +
                "soluong integer)";
        db.execSQL(sqlHoaDonChiTiet);


    }


    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("Drop table if exists NguoiDungTb");
        db.execSQL("Drop table if exists TheLoaiTb");
        db.execSQL("Drop table if exists SachTb");
        db.execSQL("Drop table if exists HoaDonTb");
        db.execSQL("Drop table if exists HoaDonChiTietTb");
        onCreate(db);
    }
}
