package com.example.ps10349_lap3;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.loader.content.CursorLoader;

import android.Manifest;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.CallLog;
import android.provider.ContactsContract;
import android.provider.MediaStore;
import android.view.View;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    final int MY_PERMISSIONS_REQUEST_READ_CONTACTS = 9;
    final int MY_PERMISSIONS_REQUEST_READ_CALL_LOG = 99;
    final int MY_PERMISSIONS_REQUEST_READ_MEDIA = 999;
    private ArrayList <String> callogs;
    private ArrayList<Uri>media;
    private ArrayList <String> contacts;
    Button btnContacts,btnCallLog,btnMedia;
    AdapterLv adapterLv ;
    AdapterLvCallLog adapterLvCallLog;
    AdapterGvMedia adapterGvMedia;
    GridView gvMedia;
    ListView lvName;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        anhxa();
        btnContacts.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                requestPermissionContacts();
                readContacts();
            }
        });
        btnCallLog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                requestPermissionCallLog();
                readCallLog();

            }
        });
        btnMedia.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                requestPermissionMedia();
                readMedia();

            }
        });


    }
    private void anhxa(){
        btnContacts = findViewById(R.id.btn_contacts);
        btnCallLog = findViewById(R.id.btn_call_logs);
        btnMedia = findViewById(R.id.btn_media);
    }
    //contacts
    public void readContacts(){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(Manifest.permission.READ_CONTACTS) != PackageManager.PERMISSION_GRANTED) {
                return;
            }
        }
        Cursor c = getContentResolver().query(ContactsContract.Contacts.CONTENT_URI,
                null,null,null,null);
        contacts = new ArrayList();
        int name = c.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME);
        while (c.moveToNext()){
           contacts.add(c.getString(name));
        }
        Toast.makeText(this, "Size"+contacts.size(), Toast.LENGTH_SHORT).show();
        adapterLv = new AdapterLv(this,contacts);
        lvName = findViewById(R.id.lv_provider);
        lvName.setAdapter(adapterLv);

    }
    private void requestPermissionContacts(){
        // Here, thisActivity is the current activity
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.READ_CONTACTS)
                != PackageManager.PERMISSION_GRANTED) {

            // Permission is not granted
            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.READ_CONTACTS)) {
                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.
            } else {
                // No explanation needed; request the permission
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.READ_CONTACTS},
                        MY_PERMISSIONS_REQUEST_READ_CONTACTS);

                // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                // app-defined int constant. The callback method gets the
                // result of the request.
            }
        } else {
            // Permission has already been granted
        }
    }
    //callLogs
    public void readCallLog(){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(Manifest.permission.READ_CALL_LOG)
                    != PackageManager.PERMISSION_GRANTED) {
                return;
            }
        }
        Cursor cursor = getContentResolver().query(CallLog.Calls.CONTENT_URI,
                        null, null, null, null);
        callogs = new ArrayList();
        int nameIndex = cursor.getColumnIndex(CallLog.Calls.NUMBER);
        while (cursor.moveToNext()){
            callogs.add(cursor.getString(nameIndex));

        }
        Toast.makeText(this, "Size: " + callogs.size(), Toast.LENGTH_SHORT).show();
        adapterLvCallLog = new AdapterLvCallLog(this,callogs);
        lvName = findViewById(R.id.lv_provider);
        lvName.setAdapter(adapterLvCallLog);

    }
    private void requestPermissionCallLog(){
        // Here, thisActivity is the current activity
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.READ_CALL_LOG)
                != PackageManager.PERMISSION_GRANTED) {

            // Permission is not granted
            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.READ_CALL_LOG)) {
                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.
            } else {
                // No explanation needed; request the permission
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.READ_CALL_LOG},
                        MY_PERMISSIONS_REQUEST_READ_CALL_LOG);

                // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                // app-defined int constant. The callback method gets the
                // result of the request.
            }
        } else {
            // Permission has already been granted
        }
    }

    //medias
    private void readMedia(){

        CursorLoader cursorLoader = new CursorLoader(this,MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                null, null, null, null);
        Cursor cursor = cursorLoader.loadInBackground();
        media = new ArrayList();
        int nameIndex = cursor.getColumnIndex(MediaStore.Images.Media.DATA);
        while (cursor.moveToNext()){
            media.add(Uri.parse(cursor.getString(nameIndex)));


        }
        adapterGvMedia = new AdapterGvMedia(this,media);
        gvMedia =findViewById(R.id.gv_provider);
        gvMedia.setAdapter(adapterGvMedia);

    }
    private void requestPermissionMedia(){
        // Here, thisActivity is the current activity
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {

            // Permission is not granted
            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.READ_EXTERNAL_STORAGE)) {
                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.
            } else {
                // No explanation needed; request the permission
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                        MY_PERMISSIONS_REQUEST_READ_MEDIA);

                // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                // app-defined int constant. The callback method gets the
                // result of the request.
            }
        } else {
            // Permission has already been granted
        }
    }



    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_READ_MEDIA:
            case MY_PERMISSIONS_REQUEST_READ_CALL_LOG:
            case MY_PERMISSIONS_REQUEST_READ_CONTACTS: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.
                } else {
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request.
        }

    }
}
