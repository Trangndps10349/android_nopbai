package com.example.ps10349_mob204.Adapter;

import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.ps10349_mob204.Model.HoaDonChiTiet;
import com.example.ps10349_mob204.R;

import java.util.ArrayList;
import java.util.List;

public class AdapterHDCT extends RecyclerView.Adapter<AdapterHDCT.ViewHolder> {
private List<HoaDonChiTiet> hoaDonChiTietList;
    private int position = -1;

    public AdapterHDCT(List<HoaDonChiTiet> hoaDonChiTietList) {
        this.hoaDonChiTietList = hoaDonChiTietList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View contactView = layoutInflater.inflate(R.layout.item_hdct, parent, false);
        return new ViewHolder(contactView);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int position) {
    final  HoaDonChiTiet hoaDonChiTiet = hoaDonChiTietList.get(position);
    holder.tvMaSach.setText(hoaDonChiTiet.getSach().getMaSach());
    holder.tvSoLuong.setText(hoaDonChiTiet.getSoLuongMua());
    holder.tvGiaBia.setText(String.valueOf(hoaDonChiTiet.getSach().getGiaBia()));
    holder.tvThanhTien.setText(String.valueOf(hoaDonChiTiet.getSach().getGiaBia()*hoaDonChiTiet.getSoLuongMua()));
    holder.itemView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                setPosition(holder.getAdapterPosition());
                return false;
            }
        });
    }
    public void setPosition(int position) {
        this.position = position;
    }
    public int getPosition() {
        return position;
    }
    @Override
    public int getItemCount() {
        return 0;
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnCreateContextMenuListener {
        TextView tvMaSach,tvSoLuong,tvGiaBia,tvThanhTien;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            tvMaSach = itemView.findViewById(R.id.tv_ma_sach_hdct);
            tvSoLuong = itemView.findViewById(R.id.tv_soluong_hdct);
            tvGiaBia = itemView.findViewById(R.id.tv_gia_bia_hdct);
            tvThanhTien = itemView.findViewById(R.id.tv_thanh_tien_hdct);
            itemView.setOnCreateContextMenuListener(this);
        }

        @Override
        public void onCreateContextMenu(ContextMenu contextMenu, View view, ContextMenu.ContextMenuInfo contextMenuInfo) {
            contextMenu.add(Menu.NONE, R.id.edit_hdct,
                    Menu.NONE, R.string.edit);
            contextMenu.add(Menu.NONE, R.id.delete_hdct,
                    Menu.NONE, R.string.delete);
        }
    }
    public void setListHDCT(List<HoaDonChiTiet> hoaDonChiTietList) {
        if (hoaDonChiTietList != null) {
            this.hoaDonChiTietList = new ArrayList<>(hoaDonChiTietList);
            notifyDataSetChanged();
        }
    }
}
