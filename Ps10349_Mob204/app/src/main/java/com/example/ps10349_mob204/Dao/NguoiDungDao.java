package com.example.ps10349_mob204.Dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.example.ps10349_mob204.Database.DbHelper;
import com.example.ps10349_mob204.Model.NguoiDung;

import java.util.ArrayList;
import java.util.List;

public class NguoiDungDao {
    private SQLiteDatabase db;
    private DbHelper dbHelper;


    public NguoiDungDao(Context context) {
        dbHelper = new DbHelper(context);
        db = dbHelper.getWritableDatabase();
    }

    //inser Nguoi Dung
    public int inserNguoiDung(NguoiDung nd) {
        ContentValues values = new ContentValues();
        values.put("username", nd.getUserName());
        values.put("password", nd.getPassWord());
        values.put("phone", nd.getSoDienThoai());
        values.put("hoten", nd.getHoVaTen());
        values.put("email", nd.getEmail());
        values.put("ngaysinh", nd.getNgaySinh());
        try {
            if (db.insert("NguoiDungTb", null, values) == -1) {
                return -1;
            }
        } catch (Exception ex) {
            Log.e("NguoiDungTb", ex.toString());
        }
        return 1;
    }

    //get Danh sach Nguoi Dung
    public List<NguoiDung> getAllNguoiDung() {
        List<NguoiDung> dsNguoiDung = new ArrayList<>();
        Cursor c = db.query("NguoiDungTb", null, null, null, null, null, null);
        c.moveToFirst();
        while (c.isAfterLast() == false) {
            NguoiDung nd = new NguoiDung();
            nd.setUserName(c.getString(0));
            nd.setPassWord(c.getString(1));
            nd.setSoDienThoai(c.getString(2));
            nd.setHoVaTen(c.getString(3));
            nd.setEmail(c.getString(4));
            nd.setNgaySinh(c.getString(5));
            dsNguoiDung.add(nd);
            Log.d("====", nd.toString());
            c.moveToNext();
        }
        c.close();
        return dsNguoiDung;
    }

    //update Nguoi Dung
    public int updateNguoiDung(NguoiDung nd) {
        ContentValues values = new ContentValues();
        values.put("username", nd.getUserName());
        values.put("password", nd.getPassWord());
        values.put("phone", nd.getSoDienThoai());
        values.put("hoten", nd.getHoVaTen());
        values.put("email", nd.getEmail());
        values.put("ngaysinh", nd.getNgaySinh());
        int result = db.update("NguoiDungTb", values, "username=?", new String[]{nd.getUserName()});
        if (result == 0) {
            return -1;
        }
        return 1;
    }

    //change password
    public int changePassWord(NguoiDung nd) {
        ContentValues values = new ContentValues();
        values.put("username", nd.getUserName());
        values.put("password", nd.getPassWord());
        int result = db.update("NguoiDungTb", values, "username=?", new String[]{nd.getUserName()});
        if (result == 0) {
            return -1;
        }
        return 1;
    }

    //update info nguoi dung
    public int updateInfoNguoiDung(NguoiDung nd) {
        ContentValues values = new ContentValues();
        values.put("phone", nd.getSoDienThoai());
        values.put("hoten", nd.getHoVaTen());
        values.put("email", nd.getEmail());
        values.put("ngaysinh", nd.getNgaySinh());
        int result = db.update("NguoiDungTb", values, "username=?", new String[]{nd.getUserName()});
        if (result == 0) {
            return -1;
        }
        return 1;
    }

    //delete
    public int deleteNguoiDungByID(String username) {
        int result = db.delete("NguoiDungTB", "username=?", new String[]{username});
        if (result == 0) {
            return -1;
        }
        return 1;
    }

    //check login
    public int checkLogin(String username, String password) {
        db = dbHelper.getReadableDatabase();
        int result = db.delete("NguoiDungTb", "username=? AND password=?", new String[]{username, password});
        if (result == 0) {
            return -1;
        }
        return 1;
    }

    public void deleteNguoiDung(NguoiDung nguoiDung) {
        db = dbHelper.getWritableDatabase();
        db.beginTransaction();
        try {

            db.delete("NguoiDungTb", "username=?", new String[]{nguoiDung.getUserName() + ""});
            db.setTransactionSuccessful();
        } catch (Exception e) {
        } finally {
            db.endTransaction();
        }
    }


}
